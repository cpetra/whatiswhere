Ti.include("./db.js");
Ti.include("./dbQueries.js");
Ti.include("./utils.js");

var lang = Titanium.Platform.locale;

Ti.include('./language.js');

// create tab group
var tabGroup = Titanium.UI.createTabGroup({    
		//barColor: '#0096de'
	//tabsBackgroundColor: '#ff0000'
});
//tabGroup.open();

//
// create base UI tab and root window
//
var win1 = Titanium.UI.createWindow({
    title: ITEMS,
    url: 'items.js',
    backgroundColor: '#fff'
});
var tab1 = Titanium.UI.createTab({
    icon: './images/tab_items.png',
    title: ITEMS,
    window: win1
});

//
// create controls tab and root window
//

var win2 = Titanium.UI.createWindow({
    title: LOCATIONS,
    url: 'locations.js'
});
var tab2 = Titanium.UI.createTab({
    icon: './images/tab_locations.png',
    title: LOCATIONS,
    window: win2
});

var win3 = Titanium.UI.createWindow({
    title: CONTACTS,
    url: 'contacts.js'
});
var tab3 = Titanium.UI.createTab({
    icon: './images/tab_contacts.png',
    title: CONTACTS,
    window: win3
});

var win4 = Titanium.UI.createWindow({
    title: TYPES,
    url: 'types.js'
});
var tab4 = Titanium.UI.createTab({
    icon: './images/tab_types.png',
    title: TYPES,
    window: win4
});


 var win5 = Titanium.UI.createWindow({
 title:SETTINGS,
 url:'settings.js'
 });
 var tab5 = Titanium.UI.createTab({
 icon:'./images/tab_settings.png',
 title:SETTINGS,
 window:win5
 });

//
//  add tabs
//
tabGroup.addTab(tab1);
tabGroup.addTab(tab2);
tabGroup.addTab(tab3);
tabGroup.addTab(tab4);
tabGroup.addTab(tab5);  


// open tab group
tabGroup.open({
    transition: Titanium.UI.iPhone.AnimationStyle.FLIP_FROM_LEFT
});

//Ti.include("./welcome.js");

Ti.include("./version.js");

if (isiOS4Plus()) {
    // register a background service. this JS will run when the app is backgrounded
    
    var service = Ti.App.iOS.registerBackgroundService({
        url: 'bg.js'
    });

    //	Ti.API.info("registered background service = "+service);
    
    // listen for a local notification event
    Ti.App.iOS.addEventListener('notification', function(e){
    // Ti.API.info("local notification received: " + JSON.stringify(e));
    });
    
    
    // fired when an app resumes for suspension
    Ti.App.addEventListener('resume', function(e){
       // Ti.API.info("app is resuming from the background");
    });
    
    Ti.App.addEventListener('resumed', function(e){
        // Ti.API.info("app has resumed from the background");
		Ti.App.iOS.cancelAllLocalNotifications();
		tabGroup.open();
	    tabGroup.setActiveTab(0);
   });
    
    Ti.App.addEventListener('pause', function(e){
        // Ti.API.info("app was paused from the foreground");
 
 		Ti.include('./notification.js');
       
//        if (Titanium.App.Properties.getString('badges_preference') == 1) {
        if (Titanium.App.Properties.getString('badgesNotification') == 1) {
            Titanium.UI.iPhone.appBadge = getNumberItemsToRemind();
        }
        else {
            Titanium.UI.iPhone.appBadge = null;
        }
   	});
}