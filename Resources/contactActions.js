
Ti.include('./language.js');


var actionsDialog = Titanium.UI.createOptionDialog({
	title: (CONTACT_ACT + ' ' + contactName),
//	options: [CALL, SENDSMS, SENDEMAIL, CANCEL],
//	cancel: 3
	options: [CALL, SENDSMS, CANCEL],
	cancel: 2
});


actionsDialog.addEventListener('click', function(e1) {
	
	var labels;

	selectedContact = Titanium.Contacts.getPeopleWithName(contactName);
	
	if ( e1.index == 0 || e1.index == 1 ) {		

		var comType = e1.index == 0 ? "tel:" : "sms:";

		var phones = selectedContact[0].phone;
		
		var phonesAndNumbers = [];
		var labelsUsedP = [];
		var i = 0;

		for (labels in phones) {
			if (phones[labels] != null) {
				i++;
				labelsUsedP[i-1] = labels;
				phonesAndNumbers[i-1] = labels + ': ' + phones[labels];
			}
		}
	
		if (i == 0) {
			alert(NO_PHONE_NUMBER + ' ' + contactName);						
		}
		else if (i == 1) {
			var telString = comType + phones[labelsUsedP[i-1]];
			telString = telString.split(' ').join('');
			Titanium.Platform.openURL(telString);			
		}
		else if (i != 0) {
			phonesAndNumbers[i] = CANCEL;
			var phonesDialog = Titanium.UI.createOptionDialog({
				title: (CHOOSE_PHONE_NUMBER + ' ' + contactName),
				options: phonesAndNumbers,
				cancel: i
			});
			phonesDialog.show();
			
			phonesDialog.addEventListener('click', function(e2) {
				if ( e2.index != i ) {
					var telString = comType + phones[labelsUsedP[e2.index]];
					telString = telString.split(' ').join('');
					Titanium.Platform.openURL(telString);					
				}
			});							
		}
	}
	else if (e1.index == 2) {
	}
	
/*
	else 
		if (e1.index == 2) {
		
			var emails = selectedContact[0].email;
			
			//		alert (emails);
			
			if (emails) {
				var emailAndAddress = [];
				var labelsUsedE = [];
				var j = 0;
				
				for (labels in emails) {
					if (emails[labels] != null) {
						j++;
						labelsUsedE[j - 1] = labels;
						emailAndAddress[j - 1] = labels + ': ' + emails[labels];
					}
				}
				
				if (j == 0) {
					alert(NO_EMAIL + ' ' + contactName);
				}
				else 
					if (j == 1) {
						var emailDialog = Titanium.UI.createEmailDialog();
						emailDialog.subject = EMAIL_SUBJECT;
						emailDialog.toRecipients = emails[labelsUsedE[j - 1]];
						if (itemName) {
							emailDialog.messageBody = EMAIL_MESSAGE1 + itemName + '. ' + EMAIL_MESSAGE2;
						}
						else {
							emailDialog.messageBody = '';
						}
						emailDialog.open();
					}
					else 
						if (j > 1) {
							emailAndAddress[j] = CANCEL;
							var listEmailsDialog = Titanium.UI.createOptionDialog({
								title: (CHOOSE_EMAIL + ' ' + contactName),
								options: emailAndAddress,
								cancel: j
							});
							listEmailsDialog.show();
							
							listEmailsDialog.addEventListener('click', function(e3){
								if (e3.index != j) {
									var emailDialog = Titanium.UI.createEmailDialog();
									emailDialog.subject = EMAIL_SUBJECT;
									emailDialog.toRecipients = emails[labelsUsedE[e3.index]];
									if (itemName) {
										emailDialog.messageBody = EMAIL_MESSAGE1 + itemName + '. ' + EMAIL_MESSAGE2;
									}
									else {
										emailDialog.messageBody = '';
									}
									emailDialog.open();
								}
							});
						}
				
				
			}
			else {
				alert(CANT_GET_EMAILS);
			}			
		}
*/
});

actionsDialog.show();