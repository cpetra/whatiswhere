currentWindow = Titanium.UI.currentWindow;
currentWindow.backgroundColor = 'white';

Ti.include('./language.js');

Ti.include("./dbQueries.js");

Ti.include("./utils.js");

/*
currentWindow.orientationModes = [
	Titanium.UI.PORTRAIT,
	Titanium.UI.UPSIDE_PORTRAIT
]; 

*/

////////////////////////////////////////////////////////////
function reloadDataContact()
{
    var db = Titanium.Database.open('wiwdb');
    var contacts = db.execute("SELECT CONTACT_ID, COUNT(*) FROM ITEMS WHERE LOC_OR_CONTACT = 'C' AND CONTACT_ID != '' GROUP BY CONTACT_ID");
    var data = [];

    // create table view data object
    while (contacts.isValidRow()) {

        var row = Ti.UI.createTableViewRow({
			height: rowHeightItems
		});
		
//		row.rowid = contacts.fieldByName('ROWID');
		
		row.contactName = contacts.fieldByName('CONTACT_ID');
		
		var contactNameLabel = Ti.UI.createLabel({
            text:row.contactName,
			font: fontStyle12,
 			color:'#576996',
			top : topLocContact,
            left:leftAfterImage,
			width: widthMax,
			height:'auto'
		});
		row.add(contactNameLabel);

		var selectedContact = Titanium.Contacts.getPeopleWithName(row.contactName);
		
        var contactPhoto = Ti.UI.createImageView({
//            image: selectedContact[0].image,
			borderRadius:10,
            top:topPhoto,
            left:leftPhoto,
            width:imageSmallWidth,
            height:imageSmallHeight
       });
		if ( selectedContact[0].image )	{
			contactPhoto.image = selectedContact[0].image;
		}		
		else {
			contactPhoto.image = Ti.Filesystem.getFile('./images/photo.png');
		}
        row.add(contactPhoto);

        var contactActions = Ti.UI.createLabel({
            font:fontStyle8,
            top:topContactActions,
            left:leftContactActions,
            width:widthContactActions,
			textAlign:'center',
			height:'auto',
			backgroundColor:'black',
			color:'white',
			opacity:0.7,
            text:ACTIONS,
			borderRadius:5
	       });
		row.add(contactActions);	
		
		var nb = contacts.field(1);
		if (nb > 0) {
			var textItem;
			if (nb == 1) {
				textItem = ' ' + ITEM;
			}
			else {
				textItem = ' ' + ITEMS;
			}
			var nbItems = Ti.UI.createLabel({
				font: fontStyle12,
				color:'#576996',
				text: nb + textItem,
				left: widthMax-widthNb-rightNbItems,
				top: topLocContact,
				width: widthNb,
				height: 'auto',
				textAlign: 'right',
				clickName: 'nb'
			});
			row.add(nbItems);
		}

        data.push(row); 

        contacts.next();
		}

    contacts.close();
    db.close();

	return data;
}

var dataContacts = reloadDataContact();
// create table view
var tableview = Titanium.UI.createTableView({
    data:dataContacts
    });

// add table view to the window
currentWindow.add(tableview);
			
tableview.addEventListener('click', function(e) {
	if (e.row) {
		if (e.source.clickName == 'nb') {
		    var win = Titanium.UI.createWindow({
	        	url:'./items.js',
				title: ITEMS_AT + e.row.contactName,
				searchValueType: 'contact',
				searchValue: e.row.contactName
		    });
	    	Ti.UI.currentTab.open(win, {modal:true});							
		}
		else {
			contactName = e.row.contactName;
//			itemName = e.row.itemName;
			Ti.include('./contactActions.js');
		}
	}
});								


currentWindow.addEventListener('focus', function() {
	var dataContacts = reloadDataContact();
	tableview.setData(dataContacts);
});

