
var lang = Titanium.Platform.locale;

var db = Titanium.Database.open('wiwdb');

/*
db.execute ('DROP TABLE ITEMS');
db.execute ('DROP TABLE TYPES');
db.execute ('DROP TABLE LOCATIONS'); 
*/
db.execute('CREATE TABLE IF NOT EXISTS ITEMS (NAME TEXT, TYPE_ID INTEGER, LOC_OR_CONTACT TEXT, LOCATION_ID INTEGER, CONTACT_ID TEXT, IMAGE_FILENAME TEXT, SINCE_DATE DATE, REMINDER_DATE DATE, CREATION_DATE DATE DEFAULT CURRENT_DATE)');

db.execute('CREATE TABLE IF NOT EXISTS TYPES (TYPE_NAME TEXT)');

db.execute('CREATE TABLE IF NOT EXISTS LOCATIONS (LOCATION_NAME TEXT, LOCATION_IMAGE_FILENAME TEXT)');

var typesList = db.execute('SELECT * FROM TYPES'); 
if (!typesList.isValidRow()) {
	switch(lang) {
		case 'fr':
			db.execute('INSERT INTO TYPES (TYPE_NAME) VALUES(?)', 'Livre');
			db.execute('INSERT INTO TYPES (TYPE_NAME) VALUES(?)', 'CD');
			db.execute('INSERT INTO TYPES (TYPE_NAME) VALUES(?)', 'DVD');
			db.execute('INSERT INTO TYPES (TYPE_NAME) VALUES(?)', 'Vêtement');
			db.execute('INSERT INTO TYPES (TYPE_NAME) VALUES(?)', 'Accessoire');
			db.execute('INSERT INTO TYPES (TYPE_NAME) VALUES(?)', 'Outil');
			db.execute('INSERT INTO TYPES (TYPE_NAME) VALUES(?)', 'Argent');
			db.execute('INSERT INTO TYPES (TYPE_NAME) VALUES(?)', 'Fichier');
			db.execute('INSERT INTO TYPES (TYPE_NAME) VALUES(?)', 'Autre');
		break;
		case 'en':
		    db.execute('INSERT INTO TYPES (TYPE_NAME) VALUES(?)', 'Book'); 
		    db.execute('INSERT INTO TYPES (TYPE_NAME) VALUES(?)', 'CD'); 
		    db.execute('INSERT INTO TYPES (TYPE_NAME) VALUES(?)', 'DVD'); 
		    db.execute('INSERT INTO TYPES (TYPE_NAME) VALUES(?)', 'Clothes'); 
			db.execute('INSERT INTO TYPES (TYPE_NAME) VALUES(?)', 'Accessories');
			db.execute('INSERT INTO TYPES (TYPE_NAME) VALUES(?)', 'Tool');
		    db.execute('INSERT INTO TYPES (TYPE_NAME) VALUES(?)', 'Money'); 
		    db.execute('INSERT INTO TYPES (TYPE_NAME) VALUES(?)', 'File'); 
		    db.execute('INSERT INTO TYPES (TYPE_NAME) VALUES(?)', 'Other');
		break;	
		case 'default':
		    db.execute('INSERT INTO TYPES (TYPE_NAME) VALUES(?)', 'Book'); 
		    db.execute('INSERT INTO TYPES (TYPE_NAME) VALUES(?)', 'CD'); 
		    db.execute('INSERT INTO TYPES (TYPE_NAME) VALUES(?)', 'DVD'); 
		    db.execute('INSERT INTO TYPES (TYPE_NAME) VALUES(?)', 'Clothes'); 
			db.execute('INSERT INTO TYPES (TYPE_NAME) VALUES(?)', 'Accessories');
			db.execute('INSERT INTO TYPES (TYPE_NAME) VALUES(?)', 'Tool');
		    db.execute('INSERT INTO TYPES (TYPE_NAME) VALUES(?)', 'Money'); 
		    db.execute('INSERT INTO TYPES (TYPE_NAME) VALUES(?)', 'File'); 
		    db.execute('INSERT INTO TYPES (TYPE_NAME) VALUES(?)', 'Other');
		break;	
	}
} 
typesList.close();

//    db.execute('INSERT INTO ITEMS (NAME, TYPE_ID, TYPE_NAME, LOCATION_ID, LOCATION_NAME, IMAGE) VALUES(?,?,?,?,?)', 'Item 1', 1, 'Type 1', 1, 'Location 1', oneImage); 
//    db.execute('INSERT INTO ITEMS (NAME, TYPE_ID, TYPE_NAME, LOCATION_ID, LOCATION_NAME, IMAGE) VALUES(?,?,?,?,?,?)', 'Item 1', 1, 'Type 1', 1, 'Location 1', oneImage); 

db.close();



var imagesItemsDirectory = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, 'images_items');
if( !imagesItemsDirectory.exists() )
{
  imagesItemsDirectory.createDirectory();
}


var locationsItemsDirectory = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, 'images_locations');
if( !locationsItemsDirectory.exists() )
{
  locationsItemsDirectory.createDirectory();
}