function deleteItemImageFilename(rowid) {
	var db = Titanium.Database.open('wiwdb');
	var directory = 'images_items';
	var filename = db.execute('SELECT IMAGE_FILENAME FROM ITEMS WHERE ROWID = ?', rowid);
	var imageFilename = filename.fieldByName('IMAGE_FILENAME');
	filename.close();
	db.close();
	imageFile = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory + '/' + directory, imageFilename);
	imageFile.deleteFile();
	imageFileLarge = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory + '/' + directory, 'L'+imageFilename);
	imageFileLarge.deleteFile();
	
}

function deleteLocationImageFilename(rowid) {
	var db = Titanium.Database.open('wiwdb');
	var directory = 'images_locations';
	var filename = db.execute('SELECT LOCATION_IMAGE_FILENAME FROM LOCATIONS WHERE ROWID = ?', rowid);
	var imageFilename = filename.fieldByName('LOCATION_IMAGE_FILENAME');
	filename.close();
	db.close();
	imageFile = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory + '/' + directory, imageFilename);
	imageFile.deleteFile();
	imageFileLarge = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory + '/' + directory, 'L'+imageFilename);
	imageFileLarge.deleteFile();
}

function deleteItemByRowid(rowid) {
	deleteItemImageFilename(rowid);
	var db = Titanium.Database.open('wiwdb');
	db.execute ('DELETE FROM ITEMS WHERE ROWID = ?', rowid);
	db.close();		
};

function deleteLocationByRowid(rowid) {
	deleteLocationImageFilename(rowid);
	var db = Titanium.Database.open('wiwdb');
	db.execute("UPDATE ITEMS SET LOCATION_ID = '', SINCE_DATE = '' , REMINDER_DATE = '' WHERE LOCATION_ID = ?", rowid);
	db.execute ('DELETE FROM LOCATIONS WHERE ROWID = ?', rowid);
	db.close();
};

function deleteTypeByRowid(rowid) {
//	Ti.API.debug('delete funct ' + rowid);
	var db = Titanium.Database.open('wiwdb');
	db.execute("UPDATE ITEMS SET TYPE_ID = '' WHERE TYPE_ID = ?", rowid);
	db.execute ('DELETE FROM TYPES WHERE ROWID = ?', rowid);
	db.close();
};

function getTypeNameById (rowid){
	var db = Titanium.Database.open('wiwdb');
	var type = db.execute('SELECT TYPE_NAME FROM TYPES WHERE ROWID = ?', rowid);
	var typeName = type.fieldByName('TYPE_NAME');
	type.close();
	db.close();	
	return typeName;
}

function getLocationNameById (rowid){
	var db = Titanium.Database.open('wiwdb');
	var location = db.execute('SELECT LOCATION_NAME FROM LOCATIONS WHERE ROWID = ?', rowid);
	var locationName = location.fieldByName('LOCATION_NAME');
	location.close();
	db.close();	
	return locationName;
}

function insertItem(itemName, typeId, locOrContact, locationId, contactId, imageFilename, sinceDate, reminderDate) {
	var db = Titanium.Database.open('wiwdb');
    db.execute('INSERT INTO ITEMS (NAME, TYPE_ID, LOC_OR_CONTACT, LOCATION_ID, CONTACT_ID, IMAGE_FILENAME, SINCE_DATE, REMINDER_DATE) VALUES(?, ?, ?, ?, ?, ?, ?, ?)',itemName, typeId, locOrContact, locationId, contactId, imageFilename, sinceDate, reminderDate);
	db.close();
};

function insertType(typeName) {
	var db = Titanium.Database.open('wiwdb');
    db.execute('INSERT INTO TYPES (TYPE_NAME) VALUES(?)',typeName);
	var rowid = db.lastInsertRowId;
	db.close();
	return rowid;
};

function insertLocation(locationName, imageFilename) {
	var db = Titanium.Database.open('wiwdb');
    db.execute('INSERT INTO LOCATIONS (LOCATION_NAME, LOCATION_IMAGE_FILENAME) VALUES(?, ?)',locationName, imageFilename);
	var rowid = db.lastInsertRowId;
	db.close();
	return rowid;
};

function updateItem(itemName, typeId, locOrContact, locationId, contactId, imageFilename, sinceDate, reminderDate, rowid) {
	var db = Titanium.Database.open('wiwdb');
	db.execute('UPDATE ITEMS SET NAME = ?, TYPE_ID = ?, LOC_OR_CONTACT = ?, LOCATION_ID = ? , CONTACT_ID = ?, IMAGE_FILENAME = ?, SINCE_DATE = ? , REMINDER_DATE = ? WHERE ROWID = ?', itemName, typeId, locOrContact, locationId, contactId, imageFilename, sinceDate, reminderDate, rowid);			
	db.close();
};

function updateType(typeName, typeId) {
	var db = Titanium.Database.open('wiwdb');
	db.execute('UPDATE TYPES SET TYPE_NAME = ? WHERE ROWID = ?', typeName, typeId);			
	db.close();
};

function updateLocation(locationName, imageFilename, locationId) {
	var db = Titanium.Database.open('wiwdb');
	db.execute('UPDATE LOCATIONS SET LOCATION_NAME = ?, LOCATION_IMAGE_FILENAME = ? WHERE ROWID = ?', locationName, imageFilename, locationId);			
	db.close();
};

function getNumberItemsByType(typeId){
	var db = Titanium.Database.open('wiwdb');
	var items = db.execute('SELECT * FROM ITEMS WHERE TYPE_ID = ?', typeId);
	var nb = items.rowCount;
	items.close();
	db.close();
	return nb;	
}

function getNumberItemsByLocation(locationId){
	var db = Titanium.Database.open('wiwdb');
	var locations = db.execute('SELECT * FROM ITEMS WHERE LOCATION_ID = ? and LOC_OR_CONTACT = "L"', locationId);
	var nb = locations.rowCount;
	locations.close();
	db.close();
	return nb;	
}

function getNumberItemsToRemind(){
	var db = Titanium.Database.open('wiwdb');
	var now_s = Math.floor(new Date().getTime()/1000);
	var items = db.execute('SELECT * FROM ITEMS WHERE REMINDER_DATE <= ?', now_s);
	var nb = items.rowCount;
	items.close();
	db.close();
	return nb;	
}

function getItemsToRemind() {

	var now_s = Math.floor(new Date().getTime()/1000);
	var sortQuery = ' ORDER BY I.REMINDER_DATE';
	var whereQuery = " WHERE REMINDER_DATE != '' and REMINDER_DATE <= ?";
	var query = 'SELECT I.ROWID, I.NAME, T.TYPE_NAME, I.LOC_OR_CONTACT, L.LOCATION_NAME, L.LOCATION_IMAGE_FILENAME, I.CONTACT_ID, I.IMAGE_FILENAME, I.SINCE_DATE, I.REMINDER_DATE FROM ITEMS I LEFT OUTER JOIN TYPES T ON I.TYPE_ID = T.ROWID LEFT OUTER JOIN LOCATIONS L ON I.LOCATION_ID = L.ROWID ';
	query = query + whereQuery + sortQuery;
	var db = Titanium.Database.open('wiwdb');
	var items = db.execute(query, now_s);		
	
	if ( items.rowCount > 0 ) {
		var itemsList = NOTIFICATION_REMIND;	
	
		while (items.isValidRow()) {
//Ti.include('./utils.js');
//Ti.API.info('getItemsToRemind Item : ' + items.fieldByName('NAME') + " " + secondsToDate(items.fieldByName('REMINDER_DATE')));
			
			var itemName = items.fieldByName('NAME') != "" ? items.fieldByName('NAME') : NO_NAME;
			itemsList = itemsList + "\n" + itemName;
			items.next();
		}

	}
	 
	items.close();
	db.close();
	
	return itemsList;
}

function updateItemsReminderTime(){
	var whereQuery = " WHERE REMINDER_DATE != ''";
	var query = 'SELECT ROWID, NAME, REMINDER_DATE FROM ITEMS ';
	query = query + whereQuery;
	var db = Titanium.Database.open('wiwdb');
	var items = db.execute(query);		
	
	while (items.isValidRow()) {
		
		var reminderDate_s = secondsToDate(items.fieldByName('REMINDER_DATE'));
		reminderDate_s.setHours(Titanium.App.Properties.getString("reminderHourNotification"),Titanium.App.Properties.getString("reminderMinuteNotification"),0);

		db.execute('UPDATE ITEMS SET REMINDER_DATE = ? WHERE ROWID = ?', reminderDate_s, items.fieldByName('ROWID'));			
		
		items.next();
	}
	 
	items.close();
	db.close();
}
