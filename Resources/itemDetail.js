currentWindow = Titanium.UI.currentWindow;
currentWindow.backgroundColor = 'white';

Ti.include('./language.js');

Ti.include("./dbQueries.js");

Ti.include("./utils.js");

Ti.include("./picker.js");

////////////////////////////////////////////////////////////
// Recuperation des valeurs de l'item clique (si c'est le cas)
var itemName, typeId, typeName, locOrContact, locationId, locationName, contactName, imageFilename, sinceDate, reminderDate;
var newTypeName;
	
if (currentWindow.title == ITEM_DETAIL) {
	var db = Titanium.Database.open('wiwdb');
	var item = db.execute('SELECT I.ROWID, I.NAME, I.TYPE_ID, I.LOC_OR_CONTACT, I.LOCATION_ID, I.CONTACT_ID, I.IMAGE_FILENAME, I.SINCE_DATE, I.REMINDER_DATE FROM ITEMS I WHERE I.ROWID = ?', currentWindow.rowid);	

	itemName = item.fieldByName('NAME');
	typeId = item.fieldByName('TYPE_ID');
	typeName = getTypeNameById(typeId);
	locOrContact = item.fieldByName('LOC_OR_CONTACT');
	locationId = item.fieldByName('LOCATION_ID');
	locationName = getLocationNameById(locationId);
	contactName = item.fieldByName('CONTACT_ID');
	imageFilename = item.fieldByName('IMAGE_FILENAME');
	sinceDate = item.fieldByName('SINCE_DATE');
	reminderDate = item.fieldByName('REMINDER_DATE');

	item.close();
	db.close();
}


//currentWindow.hideTabBar();

////////////////////////////////////////////////////////////
// Navigation Bar with a cancel and save button

Ti.include('./cancelButton.js');

cancelButton.addEventListener('click', function() {
/*
		currentWindow.navGroup.close(currentWindow.parentWindow);			
		currentWindow.navGroup.close(currentWindow);

*/
	currentWindow.close();
});


Ti.include('./saveButton.js');

////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////

var data = [];
var tableView = Ti.UI.createTableView({
//	moving:false, 	
//	scrollable: false,
	top: 0,
	style: Titanium.UI.iPhone.TableViewStyle.GROUPED
});

////////////////////////////////////////////////////////////
//////Section Item Name Photo Type

data[0] = Ti.UI.createTableViewSection();

var rowName = Ti.UI.createTableViewRow({
	backgroundColor:'white', 	
	height: imageMediumHeight + 10
	});

photoTop = topDisplay;
var directory = 'images_items';
Ti.include('managePhoto.js');
rowName.add(photo);

if (photo.image) {
	rowName.add(modifyImageLabel);	
}
else {
	rowName.add(addImageLabel);		
}

var nameTextField = Titanium.UI.createTextField({
	font: fontStyle14,
	color:'#576996',
	top : 5,
    left: 15 + imageMediumWidth,
	height:imageMediumHeight,
	width: 200,
	hintText:ITS_NAME,
	borderStyle:Titanium.UI.INPUT_BORDERSTYLE_NONE
});
rowName.add(nameTextField);
data[0].add(rowName);

var rowType = Ti.UI.createTableViewRow({
	backgroundColor:'white', 	
	height: rowHeight,
	hasChild:true
	});
var typeTextField = Titanium.UI.createTextField({
	font: fontStyle12,
	color:'#576996',
    paddingLeft: 10,
    enabled : false,
	hintText:ITS_TYPE,
	borderStyle:Titanium.UI.INPUT_BORDERSTYLE_NONE
});
rowType.add(typeTextField);
data[0].add(rowType);


nameTextField.value = itemName;
typeTextField.id = typeId;
typeTextField.value = typeName;


////////////////////////////////////////////////////////////
//////Section Location Contact

data[1] = Ti.UI.createTableViewSection();
var locationHeaderLabel = Titanium.UI.createLabel({
	text: WHERE_IS_IT,
	textAlign: 'center',
	height : 30,
	font: fontStyle14,
	color:'#576996'
});
data[1].headerView = locationHeaderLabel;

var rowLoOrContactTabbedBar = Ti.UI.createTableViewRow({
	height: rowHeight,
	backgroundColor:'transparent' 	
});

var locationOrContactTabbedBar = Titanium.UI.createTabbedBar({
	labels:[SOMEWHERE, AT_CONTACT],
    font:fontStyle12,
	height: rowHeight,
	style:Titanium.UI.iPhone.SystemButtonStyle.BAR
});
rowLoOrContactTabbedBar.add(locationOrContactTabbedBar);
data[1].add(rowLoOrContactTabbedBar);


var rowLocOrContact = Ti.UI.createTableViewRow({
	height: rowHeight,
	backgroundColor:'white', 	
	hasChild: true
});
var locOrContactTextField = Titanium.UI.createTextField({
	font: fontStyle12,
	color:'#576996',
	paddingLeft: 10,
	enabled: false,
	borderStyle: Titanium.UI.INPUT_BORDERSTYLE_NONE
});
rowLocOrContact.add(locOrContactTextField);
data[1].add(rowLocOrContact);			

////////////////////////////////////////////////////////////
function displayLocContactView(locOrContact) {

	if (locOrContact == 'L') {		
		locationOrContactTabbedBar.index = 0;
		locOrContactTextField.locOrContact = 'L';
		locOrContactTextField.hintText = WHERE;
		locOrContactTextField.id = locationId;	
		locOrContactTextField.value = locationName;
	}
	else if (locOrContact == 'C') {
		locationOrContactTabbedBar.index = 1;
		locOrContactTextField.locOrContact = 'C';
		locOrContactTextField.hintText = WHO;
		locOrContactTextField.value = contactName;
	}		
}

locOrContact = locOrContact ? locOrContact : 'L';
displayLocContactView(locOrContact);

////////////////////////////////////////////////////////////
//////Section Date Reminder

data[2] = Ti.UI.createTableViewSection();
var sinceHeader = Ti.UI.createView({
});
var sinceHeaderLabel = Titanium.UI.createLabel({
	text: SINCE_WHEN,
	textAlign: 'center',
	height : 30,
	font: fontStyle14,
	color:'#576996'
});
data[2].headerView = sinceHeaderLabel;


var rowSince = Ti.UI.createTableViewRow({
	backgroundColor:'white', 	
	height: rowHeight,
	hasChild:true
	});
var sinceLabel = Ti.UI.createLabel({
	text: SINCE,
    font:fontStyle12,
	left: 10
});
var sinceTextField = Titanium.UI.createTextField({
	font: fontStyle12,
	color:'#576996',
	textAlign: 'right',
	paddingRight:10,
    enabled : false,
	borderStyle:Titanium.UI.INPUT_BORDERSTYLE_NONE
});

var now = new Date();
if (sinceDate) {
	sinceTextField.date_s = sinceDate;
	sinceTextField.value = secondsToLocaleDateString(sinceDate);
}
else {
	if (currentWindow.title == ITEM_DETAIL) {
		sinceTextField.date_s = sinceDate;
		sinceTextField.value = NO_DATE;		
	}
	else {
		sinceTextField.date_s = Math.floor(now.getTime()/1000);
		sinceTextField.value = TODAY;			
	}
}

rowSince.add(sinceLabel);
rowSince.add(sinceTextField);
data[2].add(rowSince);

var rowReminder = Ti.UI.createTableViewRow({
	backgroundColor:'white', 	
	height: rowHeight,
	hasChild:true
	});
var reminderLabel = Ti.UI.createLabel({
	text: REMIND_ME,
    font:fontStyle12,
	left: 10
	});
var reminderTextField = Titanium.UI.createTextField({
	font: fontStyle12,
	color:'#576996',
 	textAlign: 'right',
	paddingRight:10,
    enabled : false,
	borderStyle:Titanium.UI.INPUT_BORDERSTYLE_NONE
});

if ( reminderDate ) {
	reminderTextField.date_s = reminderDate;
	reminderTextField.value = secondsToLocaleDateString(reminderDate);
	}
else {
	reminderTextField.date_s = '';
	reminderTextField.value = NEVER;	
}

rowReminder.add(reminderLabel);
rowReminder.add(reminderTextField);
data[2].add(rowReminder);


////////////////////////////////////////////////////////////
tableView.data = data;
currentWindow.add(tableView);
////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
////////////// Gestion des evenements /////////////////////////////
locationOrContactTabbedBar.addEventListener('click',function(e) {
	locOrContact = (e.index == 0) ? 'L' : 'C';
	displayLocContactView(locOrContact);
});


// evenement sur click Type
rowName.addEventListener('click', function(e)
{
	if ( e.source.clickName == 'addPhoto' ) {
	    photoDialog.show();
	}
});

// evenement sur click Type
rowType.addEventListener('click', function(e)
{
    var selectTypeWindow = Titanium.UI.createWindow({
        title: SELECT_TYPE,
        url: 'types.js'
        });	
	selectTypeWindow.open({modal:true});

/*
	var navGroup = Ti.UI.iPhone.createNavigationGroup({
	    window : selectTypeWindow
	});

	var mainWindow = Titanium.UI.createWindow();

	selectTypeWindow.navGroup = navGroup;
	selectTypeWindow.parentWindow = mainWindow;

	mainWindow.add(navGroup);
	mainWindow.open({
		transition:Titanium.UI.iPhone.AnimationStyle.NONE		
	});

*/
/*
	selectTypeWindow.navGroup = currentWindow.navGroup;
	currentWindow.navGroup.open(selectTypeWindow);

*/});

// gestion de l'evenement de selection du type	
Ti.App.addEventListener('sendSelectedTypeEvent', function(e) {
	if (e.selectedTypeId) {
		typeTextField.value = getTypeNameById(e.selectedTypeId);
		typeTextField.id = e.selectedTypeId;
	}
});

rowLocOrContact.addEventListener('click', function(e) {

	if (locOrContactTextField.locOrContact == 'L') {
		var selectLocationWindow = Titanium.UI.createWindow({
			title: SELECT_LOCATION,
			url: 'locations.js'
		});
		selectLocationWindow.open({modal:true});
/*
		selectLocationWindow.navGroup = currentWindow.navGroup;
		currentWindow.navGroup.open(selectLocationWindow);

*/	}
	else 
		if (locOrContactTextField.locOrContact == 'C') {
			Titanium.Contacts.showContacts({
				animated: true,
				selectedPerson: function(e){
					locOrContactTextField.value = e.person.fullName;
				}
			});
		}
});
// gestion de l'evenement de selection de la location	
Ti.App.addEventListener('sendSelectedLocationEvent', function(e) {
	if (e.selectedLocationId) {
		locOrContactTextField.value = getLocationNameById(e.selectedLocationId);
		locOrContactTextField.id = e.selectedLocationId;
	}
});	

// evenement gestion de la date	
rowSince.addEventListener('click', function(e) {
	displayPickerView('Since', sinceTextField.date_s);
});


rowReminder.addEventListener('click', function(e) {
	displayPickerView('Reminder', reminderTextField.date_s);
});

		
// Click sur Save (Insert ou Update)
saveButton.addEventListener('click', function() {
	
 	if (nameTextField.value == '' && photo.image == null) {
		alert(NAME_OR_IMAGE_MANDATORY);
	}
	else {
		var newImageFilename;
		var newImageFile;
		var imageFile;

		var sinceDate = sinceTextField.date_s;
		var reminderDate = reminderTextField.date_s;
		
		if (currentWindow.title == ADD_ITEM) {
			
			if (imageHasChanged) {
				newImageFilename = nowYYYYMMMDD() +'.jpg';
				newImageFile = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory + '/' + directory, newImageFilename);
				newImageFile.write(photo.toImage()); 	
				
				newImageFilenameLarge = 'L' + nowYYYYMMMDD() +'.jpg';
				newImageFileLarge = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory + '/' + directory, newImageFilenameLarge);
				newImageFileLarge.write(photoL.toImage()); 	
																
			}
			else {
				newImageFilename = '';
			}
			
			insertItem(nameTextField.value, typeTextField.id, locOrContact, locOrContactTextField.id, locOrContactTextField.value, newImageFilename, sinceDate, reminderDate);
		}
		else if (currentWindow.title == ITEM_DETAIL) {
		
			if (imageHasBeenRemoved) {
				if (imageFilename) {
					imageFile = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory + '/' + directory, imageFilename);
					imageFile.deleteFile();

					imageFileLarge = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory + '/' + directory, 'L'+imageFilename);
					imageFileLarge.deleteFile();

					imageFilename = '';						
				}
			}
			
			if (imageHasChanged) {
				if (imageFilename) {
					imageFile = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory + '/' + directory, imageFilename);
					imageFile.deleteFile();
					imageFilename = '';							
				}
				
				newImageFilename = nowYYYYMMMDD() + '.jpg';
				newImageFile = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory + '/' + directory, newImageFilename);
				newImageFile.write(photo.toImage());
				imageFilename = newImageFilename;

				newImageFilenameLarge = 'L' + nowYYYYMMMDD() +'.jpg';
				newImageFileLarge = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory + '/' + directory, newImageFilenameLarge);
				newImageFileLarge.write(photoL.toImage()); 		
			}
			updateItem(nameTextField.value, typeTextField.id, locOrContact, locOrContactTextField.id, locOrContactTextField.value, imageFilename, sinceDate, reminderDate, currentWindow.rowid);
		}

		currentWindow.close();

/*
		currentWindow.navGroup.close(currentWindow.parentWindow);
		currentWindow.navGroup.close(currentWindow);

*/
	}
});
///////////////////////////////////////////////////////////////////