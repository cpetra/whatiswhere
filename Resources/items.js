currentWindow = Titanium.UI.currentWindow;
currentWindow.backgroundColor = 'white';

Ti.API.debug("Items");

Ti.include('./language.js');
Ti.include("./utils.js");

var selectedContact;
var sortType, sortQuery = '';

Ti.include("./dbQueries.js");

// This is to show the navBar if it has been hidden when selecting dates
if (currentWindow.navBar != 'visible' ) {
	currentWindow.showNavBar();
}

if (!currentWindow.searchValueType) {
	Ti.include('./sortItems.js');
	Ti.include('./addButton.js');	
}
Ti.include('./search.js');


//Ti.include('./itemsCoverflow.js');
/*
Ti.API.info("REMINDER REMINDER REMINDER");
// This is to opeon on Item Detail if we come from a notification reminder

Ti.API.info(currentWindow.itemId);
if ( currentWindow.itemId ) {
	openItemDetailWindow(currentWindow.itemId);
}

*/
function reloadData()
{
	switch (sortType) {
		case 'item':
		sortQuery = ' ORDER BY I.NAME';
		break;
		case 'location':
		sortQuery = ' ORDER BY L.LOCATION_NAME, I.CONTACT_ID';
		break;
		case 'type':
		sortQuery = ' ORDER BY T.TYPE_NAME';
		break;
		case 'creationDate':
		sortQuery = ' ORDER BY I.CREATION_DATE';
		break;
		case 'sinceDate':
		sortQuery = ' ORDER BY I.SINCE_DATE';
		break;
		case 'default':
		sortQuery = ' ORDER BY I.CREATION_DATE';
		break;		
	}

	var whereQuery;
	switch (currentWindow.searchValueType) {
		case 'contact':
		whereQuery = " WHERE I.CONTACT_ID = '" + currentWindow.searchValue + "'";
		break;
		case 'location':
		whereQuery = " WHERE I.LOCATION_ID = '" + currentWindow.searchValue + "'";
		break;
		case 'type':
		whereQuery = " WHERE I.TYPE_ID = '" + currentWindow.searchValue + "'";
		break;
		case 'name':
		whereQuery = " WHERE I.NAME = '" + currentWindow.searchValue + "'";
		break;
		default:
		whereQuery = "";
		break;
	}

	var query = 'SELECT I.ROWID, I.NAME, T.TYPE_NAME, I.LOC_OR_CONTACT, L.LOCATION_NAME, L.LOCATION_IMAGE_FILENAME, I.CONTACT_ID, I.IMAGE_FILENAME, I.SINCE_DATE, I.REMINDER_DATE, I.CREATION_DATE FROM ITEMS I LEFT OUTER JOIN TYPES T ON I.TYPE_ID = T.ROWID LEFT OUTER JOIN LOCATIONS L ON I.LOCATION_ID = L.ROWID ';
	
	query = query + whereQuery + sortQuery;
	
    var db = Titanium.Database.open('wiwdb');
	var items = db.execute(query);		

	var data = [];

	var now_s = Math.floor(new Date().getTime()/1000);
 
    // create table view data object
    while (items.isValidRow())
    {
        var row = Ti.UI.createTableViewRow({
        	height: rowHeightItems		
		});

		var containerView = Ti.UI.createView ();

		row.rowid = items.fieldByName('ROWID');

        var itemPhoto = Ti.UI.createImageView({
			borderRadius:10,
            top:topPhoto,
            left:leftPhoto,
            width:imageSmallWidth,
            height:imageSmallHeight
       });			
		
		var imageFile;
		if (items.fieldByName('IMAGE_FILENAME')) {
			imageFile = Ti.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory + '/images_items',items.fieldByName('IMAGE_FILENAME'));
		}
		else {
			imageFile = Ti.Filesystem.getFile('./images/photo.png');			
		}
		itemPhoto.image = imageFile;

		row.itemName = items.fieldByName('NAME');
        var name = Ti.UI.createLabel({
			font: fontStyle12,
			color:'#576996',
            left:leftAfterImage,
	        top:0,
            height:30,
            width:146,
            text: row.itemName
        });

	        var type = Ti.UI.createLabel({
				font: fontStyle8,
				backgroundColor:'#576996',
	//			color:'#576996',
				color:'white',
	            left:leftAfterImage,
	            top:topFirstLine+imageSmallHeight-12,
	            height:'auto',
	            width:'auto',
				borderRadius:3,
	            text:' '+ items.fieldByName('TYPE_NAME')+ ' '
	        });
	
		if (items.fieldByName('TYPE_NAME')) {
			containerView.add(type);			
		}


		if (items.fieldByName('SINCE_DATE') && (items.fieldByName('LOCATION_NAME') || items.fieldByName('CONTACT_ID'))) {
			var nbDays = nbDaysBetween(new Date(),secondsToDate(items.fieldByName('SINCE_DATE')));
	
	        var nbDaysSinceLabel = Ti.UI.createLabel({
	            text: SINCE + ' ' + nbDays + ' ' + (nbDays > 1 ? DAYS : DAY),
				font: fontStyle8,
				backgroundColor:'#425A19',
				color:'white',
	            left:widthMax-rightPhoto-widthContactActions-widthNb,
	            top:topFirstLine,
	            height:'auto',
	            width:widthNb,
				borderRadius:3,
				textAlign: 'center'
	        });

			containerView.add(nbDaysSinceLabel);			
			
			if (items.fieldByName('REMINDER_DATE')) {
				if (now_s >= items.fieldByName('REMINDER_DATE')) {
					nbDaysSinceLabel.backgroundColor = 'red';
					nbDaysSinceLabel.text = nbDaysSinceLabel.text + ' ' + REMIND;
				}							
			}
		}
		
		if (items.fieldByName('LOC_OR_CONTACT') == 'L') {
			var location = Ti.UI.createLabel({
				text: items.fieldByName('LOCATION_NAME'),
				font: fontStyle12,
				color: '#576996',
				textAlign: 'right',
	            left:0,
	            height:'auto',
	            width:widthMax-rightPhoto-widthContactActions,
				top: topFirstLine+imageSmallHeight-12
			});
			
			var locationPhoto = Ti.UI.createImageView({
				borderRadius:10,
				top: topPhoto,
	            left:widthMax-rightPhoto-imageSmallWidth,
	            width:imageSmallWidth,
	            height:imageSmallHeight
			});

			var locationImageFile;
			if (items.fieldByName('LOCATION_IMAGE_FILENAME')) {
				locationImageFile = Ti.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory + '/images_locations', items.fieldByName('LOCATION_IMAGE_FILENAME'));
				locationPhoto.image = locationImageFile;
			}
			else if (items.fieldByName('LOCATION_NAME')){
				locationImageFile = Ti.Filesystem.getFile('./images/photo.png');						
				locationPhoto.image = locationImageFile;
			}			
		}

		if (items.fieldByName('LOC_OR_CONTACT') == 'C') {
			row.contactName = items.fieldByName('CONTACT_ID');
	         var contact = Ti.UI.createLabel({
	            color:'#576996',
				font: fontStyle12,
				textAlign:'right',
	            left:0,
	            width:widthMax-rightPhoto-widthContactActions,
				top: topFirstLine+imageSmallHeight-12,
	            height:'auto',
	            text:AT + row.contactName,
				clickName: 'contactActions'
	        });

			selectedContact = Titanium.Contacts.getPeopleWithName(row.contactName);
	        var contactPhoto = Ti.UI.createImageView({
//	            image: selectedContact[0].image,
				borderRadius:10,
	            top:topPhoto,
	            left:widthMax-rightPhoto-imageSmallWidth,
	            width:imageSmallWidth,
	            height:imageSmallHeight,
				clickName: 'contactActions'
	        });	

			if ( selectedContact[0].image )	{
				contactPhoto.image = selectedContact[0].image;
			}		
			else {
				contactPhoto.image = Ti.Filesystem.getFile('./images/photo.png');
			}
			
	        var contactActions = Ti.UI.createLabel({
	            font:fontStyle8,
	            top:topContactActions,
	            left:widthMax-widthContactActions-rightContactActions,
	            width:widthContactActions,
				textAlign:'center',
				height:'auto',
				backgroundColor:'black',
				color:'white',
				opacity:0.7,
	            text:ACTIONS,
				borderRadius:5,
				clickName: 'contactActions'
		       });

		}
					
	        containerView.add(itemPhoto);
	        containerView.add(name);

	        if (items.fieldByName('LOC_OR_CONTACT') == 'L') {
				containerView.add(location);
				containerView.add(locationPhoto);						
				row.filter = name.text + type.text + location.text;
	
			}
			else if (items.fieldByName('LOC_OR_CONTACT') == 'C') {
				containerView.add(contact);
				containerView.add(contactPhoto);
				containerView.add(contactActions);	
				row.filter = name.text + type.text + contact.text;
			}

			row.add(containerView);

	        data.push(row);
	        items.next();			

    }
    items.close();
    db.close();

	var rowNewEntry = Ti.UI.createTableViewRow({
		height: rowHeightItems		
	});	
	var newEntry = Ti.UI.createLabel({
		text: NEW_ITEM,
		font: fontStyle14,
		color: '#576996',
		clickName: 'new',
		left: leftTypes,
		top: topTypes,
		width: widthMax,
		height:'auto',
		textAlign: 'left'
	});
	rowNewEntry.add(newEntry);
	data.push(rowNewEntry);
		
	return data;
}


var data1 = reloadData();

var tableview = Titanium.UI.createTableView({
	search:search,
	filterAttribute:'filter',
	data:data1,
	deleteButtonTitle:DELETE,
	editable:true
});

currentWindow.add(tableview);

tableview.addEventListener('delete',function(e) {
	deleteItemByRowid(e.row.rowid);
 });


currentWindow.addEventListener('focus', function() {
		var updatedData = reloadData();
		tableview.setData(updatedData);
});
			 
// Click on row => display Item details
// if click on Actions, dislay contact actions

function openItemDetailWindow(rowid) {

	var win = Titanium.UI.createWindow({
		url:'itemDetail.js',
		title: rowid ? ITEM_DETAIL : ADD_ITEM,
		rowid: rowid ? rowid : ''
	});				

//	win.open({modal:true});

/*
	var navGroup = Ti.UI.iPhone.createNavigationGroup({
	    window : win
	});

	var mainWindow = Titanium.UI.createWindow();

	win.navGroup = navGroup;
	win.parentWindow = mainWindow;

	mainWindow.add(navGroup);
	mainWindow.open({
		transition:Titanium.UI.iPhone.AnimationStyle.NONE		
	});
*/
    win.addEventListener('close', function() {
		var updatedData = reloadData();
		tableview.setData(updatedData);
		});

	Titanium.UI.currentTab.open(win,{animated:true});	
}

tableview.addEventListener('click',function(e)
{
	if (e.source.clickName == 'contactActions') {
			contactName = e.row.contactName;
			itemName = e.row.itemName;
			Ti.include('./contactActions.js');
	}
	else {
		if (e.row) {
			openItemDetailWindow(e.row.rowid);
		}
	}
 });

if (!currentWindow.searchValueType) {
	add.addEventListener('click', function() {
		openItemDetailWindow('');
	 });
}

