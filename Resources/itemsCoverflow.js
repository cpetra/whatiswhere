var indexCoverflow;

function getItemDetails(index) {

	var j = 0;
	var sortQuery = ' ORDER BY I.NAME';
	var query = 'SELECT I.ROWID, I.NAME, T.TYPE_NAME, I.LOC_OR_CONTACT, L.LOCATION_NAME, L.LOCATION_IMAGE_FILENAME, I.CONTACT_ID, I.IMAGE_FILENAME, I.SINCE_DATE, I.REMINDER_DATE FROM ITEMS I LEFT OUTER JOIN TYPES T ON I.TYPE_ID = T.ROWID LEFT OUTER JOIN LOCATIONS L ON I.LOCATION_ID = L.ROWID '+ sortQuery;
	var db = Titanium.Database.open('wiwdb');
	var items = db.execute(query);		

	for (j = 0; j < index; j++) {
		items.next();
	}

	itemName = items.fieldByName('NAME');
	typeName = items.fieldByName('TYPE_NAME');
	locOrContact = items.fieldByName('LOC_OR_CONTACT');
	locationName = items.fieldByName('LOCATION_NAME');
	locationImageFilename = items.fieldByName('LOCATION_IMAGE_FILENAME');
	contactName = items.fieldByName('CONTACT_ID');
	sinceDate = items.fieldByName('SINCE_DATE');
	reminderDate = items.fieldByName('REMINDER_DATE');

	items.close();
	db.close();
	
	return [itemName, typeName, locOrContact, locationName, locationImageFilename, contactName, sinceDate, reminderDate];
}

var coverflowWindow = Titanium.UI.createWindow({
});

//coverflowWindow.open();

var containerView = Titanium.UI.createView({
	backgroundColor:'#000'
});

var coverflowView = Titanium.UI.createCoverFlowView({
});


Ti.Gesture.addEventListener('orientationchange',function(e)
{
	Titanium.UI.orientation = Titanium.Gesture.orientation;

	if (Titanium.Gesture.orientation == Titanium.UI.LANDSCAPE_LEFT || Titanium.Gesture.orientation == Titanium.UI.LANDSCAPE_RIGHT ) {

		Ti.include('./language.js');
		Ti.include("./dbQueries.js");	
		
		var sortQuery = 'ORDER BY I.NAME';
		var query = 'SELECT I.ROWID, I.NAME, T.TYPE_NAME, I.LOC_OR_CONTACT, L.LOCATION_NAME, L.LOCATION_IMAGE_FILENAME, I.CONTACT_ID, I.IMAGE_FILENAME, I.SINCE_DATE, I.REMINDER_DATE, I.CREATION_DATE FROM ITEMS I LEFT OUTER JOIN TYPES T ON I.TYPE_ID = T.ROWID LEFT OUTER JOIN LOCATIONS L ON I.LOCATION_ID = L.ROWID ';
		query = query + sortQuery;

		var db = Titanium.Database.open('wiwdb');
		var items = db.execute(query);		
		
		var images = [];
		var j = 0;
		
		// create table view data object
		while (items.isValidRow()) {
			if (items.fieldByName('IMAGE_FILENAME')) {
				images[j] = Titanium.Filesystem.applicationDataDirectory + '/images_items/L' + items.fieldByName('IMAGE_FILENAME');	
			}
			else {
				images[j] = Ti.Filesystem.getFile('./images/Lphoto.png');					
			}
			j++;		
 	       items.next();			
		}
		
		items.close();
		db.close();

		var containerView2 = Ti.UI.createView ({
//			backgroundColor:'black',
			left : 0,
			bottom:0,
			height:'auto',
			opacity:1
		});

	    var itemNameLabel = Ti.UI.createLabel({
			font: fontStyle14,
			color:'white',
			height:'auto',
			left: 10,
		    bottom:40
		});
		var typeNameLabel = Ti.UI.createLabel({
			font: fontStyle10,
			color:'white',
			height: 'auto',
			left: 10,
	        bottom:20
		});

		var nbDays;
        var nbDaysSinceLabel = Ti.UI.createLabel({
			font: fontStyle12,
			backgroundColor:'#425A19',
			color:'white',
            right:10,
            top:10,
            height:'auto',
            width:90,
			borderRadius:3,
			textAlign: 'center'
        });

		var locationNameLabel = Ti.UI.createLabel({
			color:'white',
			font: fontStyle14,
			textAlign: 'right',
			right: 10,
			bottom: 0,
			height: 'auto',
			width: 200
		});
		
		var locationPhoto = Ti.UI.createImageView({
			font: fontStyle14,
			borderRadius:10,
			bottom: 20,
			right: 10,
			width: imageSmallWidth,
			height: imageSmallHeight
		});

         var contactNameLabel = Ti.UI.createLabel({
			color:'white',
			font: fontStyle14,
			textAlign:'right',
            right:10,
			bottom: 0,
            height:20,
            width:200
        });

        var contactPhoto = Ti.UI.createImageView({
			borderRadius:10,
            bottom:20,
            right:10,
			width: imageSmallWidth,
			height: imageSmallHeight,
			clickName: 'contactActions'
			});		


        var contactActions = Ti.UI.createLabel({
			font: fontStyle8,
			color:'white',
            bottom:20,
            right:8,
            width:widthContactActions,
			textAlign:'center',
			height:'auto',
			backgroundColor:'black',
			opacity:0.7,
            text:ACTIONS,
			borderRadius:5,
			clickName: 'contactActions'
	       });


		currentWindow.hideNavBar();
//		currentWindow.hideTabBar();

        containerView2.add(itemNameLabel);
        containerView2.add(typeNameLabel);
		containerView2.add(locationNameLabel);
		containerView2.add(locationPhoto);						
		containerView2.add(contactNameLabel);
		containerView2.add(contactPhoto);
//		containerView2.add(contactActions);	

		containerView.add(nbDaysSinceLabel);			


		containerView.add(coverflowView);

		containerView.add(containerView2);

		currentWindow.add(containerView);
					
		coverflowView.images = images;		
		
		itemNameLabel.text = getItemDetails(indexCoverflow)[0];
		typeNameLabel.text = getItemDetails(indexCoverflow)[1];
		locOrContact = getItemDetails(indexCoverflow)[2];
		
		if (locOrContact == 'L') {
			contactNameLabel.text = '';						
			contactPhoto.hide();
			contactActions.hide();
			locationPhoto.show();			
			locationNameLabel.text = getItemDetails(indexCoverflow)[3];
			locationImageFilename = getItemDetails(indexCoverflow)[4];
			var locationImageFile;
			if (locationImageFilename) {
				locationImageFile = Ti.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory + '/images_locations',locationImageFilename);
			}
			else {
				locationImageFile = Ti.Filesystem.getFile('./images/photo.png');					
			}
			locationPhoto.image = locationImageFile;			
		}
		else if (locOrContact == 'C') {
			locationNameLabel.text = '';
			locationPhoto.hide();
			contactPhoto.show();			
			contactNameLabel.text = getItemDetails(indexCoverflow)[5];
			selectedContact = Titanium.Contacts.getPeopleWithName(contactNameLabel.text);
			var contactImageFile;
			if (selectedContact[0].image) {
	            contactImageFile = selectedContact[0].image;				
			}
			else {
	            contactImageFile = Ti.Filesystem.getFile('./images/photo.png');				
			}
			contactPhoto.image = contactImageFile;

			contactActions.show();
			contactActions.addEventListener('click',function(e) {
				contactName = contactNameLabel.text;
				itemName = itemNameLabel.text;
				Ti.include('./contactActions.js');
			});
		}

		sinceDate = getItemDetails(indexCoverflow)[6];
		reminderDate = getItemDetails(indexCoverflow)[7];				
		if (sinceDate) {
			nbDays = nbDaysBetween(new Date(),secondsToDate(sinceDate));
			nbDaysSinceLabel.text = SINCE + ' ' + nbDays + ' ' + (nbDays > 1 ? DAYS : DAY);
			
			if (reminderDate) {
				if (now_s >= reminderDate) {
					nbDaysSinceLabel.backgroundColor = 'red';
					nbDaysSinceLabel.text = nbDaysSinceLabel.text + ' ' + REMIND;
				}							
			}
		}	

		coverflowView.addEventListener('change',function(e) {
			indexCoverflow = e.index;	
			itemNameLabel.text = getItemDetails(indexCoverflow)[0];
			typeNameLabel.text = getItemDetails(indexCoverflow)[1];
			locOrContact = getItemDetails(indexCoverflow)[2];
			
			if (locOrContact == 'L') {
				contactNameLabel.text = '';						
				contactPhoto.hide();
				contactActions.hide();
				locationPhoto.show();			
				locationNameLabel.text = getItemDetails(indexCoverflow)[3];
				locationImageFilename = getItemDetails(indexCoverflow)[4];
				var locationImageFile;
				if (locationImageFilename) {
					locationImageFile = Ti.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory + '/images_locations',locationImageFilename);
				}
				else {
					locationImageFile = Ti.Filesystem.getFile('./images/photo.png');					
				}
				locationPhoto.image = locationImageFile;			
			}
			else if (locOrContact == 'C') {
				locationNameLabel.text = '';
				locationPhoto.hide();
				contactPhoto.show();			
				contactNameLabel.text = getItemDetails(indexCoverflow)[5];
				selectedContact = Titanium.Contacts.getPeopleWithName(contactNameLabel.text);
				var contactImageFile;
				if (selectedContact[0].image) {
		            contactImageFile = selectedContact[0].image;				
				}
				else {
		            contactImageFile = Ti.Filesystem.getFile('./images/photo.png');				
				}
				contactPhoto.image = contactImageFile;
	
				contactActions.show();
				contactActions.addEventListener('click',function(e) {
					contactName = contactNameLabel.text;
					itemName = itemNameLabel.text;
					Ti.include('./contactActions.js');
				});
			}
	
			sinceDate = getItemDetails(indexCoverflow)[6];
			reminderDate = getItemDetails(indexCoverflow)[7];				
			if (sinceDate) {
				nbDays = nbDaysBetween(new Date(),secondsToDate(sinceDate));
				nbDaysSinceLabel.text = SINCE + ' ' + nbDays + ' ' + (nbDays > 1 ? DAYS : DAY);
				
				if (reminderDate) {
					if (now_s >= reminderDate) {
						nbDaysSinceLabel.backgroundColor = 'red';
						nbDaysSinceLabel.text = nbDaysSinceLabel.text + ' ' + REMIND;
					}							
				}
			}	
		});
	}
	else {
//		Titanium.UI.orientation = Titanium.UI.PORTRAIT;
//		coverflowWindow.close();
		currentWindow.remove(containerView);
		currentWindow.showNavBar();
//		currentWindow.showTabBar();
	}		
});



