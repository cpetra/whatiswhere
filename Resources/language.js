/**
 * @author guillaume
 */
var lang = Titanium.Platform.locale;

if (lang != 'en' && lang != 'fr') {
	Ti.include('./lang/en.js');		
}
else {
	Ti.include('./lang/'+lang+'.js');	
}	
