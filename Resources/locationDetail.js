currentWindow = Titanium.UI.currentWindow;

Ti.include('./language.js');

Ti.include("./dbQueries.js");

Ti.include("./utils.js");

////////////////////////////////////////////////////////////
// Navigation Bar with a cancel and save button

Ti.include('./cancelButton.js');

cancelButton.addEventListener('click', function() {
	currentWindow.close();
});

Ti.include('./saveButton.js');

////////////////////////////////////////////////////////////
// Recuperation des valeurs de la location clique (si c'est le cas)
var locationName, imageFilename;

if (currentWindow.title == LOCATION_DETAIL) {

	var db = Titanium.Database.open('wiwdb');
	var locationSelected = db.execute('SELECT LOCATION_NAME, LOCATION_IMAGE_FILENAME FROM LOCATIONS WHERE ROWID = ?', currentWindow.locationId);

	locationName = locationSelected.fieldByName('LOCATION_NAME');
	imageFilename = locationSelected.fieldByName('LOCATION_IMAGE_FILENAME');

	locationSelected.close();
	db.close();
}

////////////////////////////////////////////////////////////
var data = [];
var tableView = Ti.UI.createTableView({
	moving:false, 	
	scrollable: false,
	top: 0,
	style: Titanium.UI.iPhone.TableViewStyle.GROUPED
});

////////////////////////////////////////////////////////////
//////Section Item Name Photo Type

data[0] = Ti.UI.createTableViewSection();

var rowName = Ti.UI.createTableViewRow({
	backgroundColor:'white', 	
	height: imageMediumHeight + 10
	});

photoTop = topDisplay;
var directory = 'images_locations';
Ti.include('managePhoto.js');
rowName.add(photo);

if (photo.image) {
	rowName.add(modifyImageLabel);	
}
else {
	rowName.add(addImageLabel);		
}

var locationTextField = Titanium.UI.createTextField({
	font: fontStyle14,
	color:'#576996',
    left: 15 + imageMediumWidth,
	hintText:ITS_NAME,
	borderStyle:Titanium.UI.INPUT_BORDERSTYLE_NONE
});
rowName.add(locationTextField);
data[0].add(rowName);

locationTextField.value = locationName;
locationTextField.id = currentWindow.locationId;

////////////////////////////////////////////////////////////
tableView.data = data;
currentWindow.add(tableView);
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Add Map Button - TO DO
/*
var addMapButton = Titanium.UI.createButton({
	title:'Add Map',
	textAlign:"center",
	height:40,
	width:200,
	top:200
});

addMapButton.addEventListener('click',function(e) {
});

currentWindow.add(addMapButton);
*/
////////////////////////////////////////////////////////////

// evenement sur click Location
rowName.addEventListener('click', function(e)
{
	if ( e.source.clickName == 'addPhoto' ) {
	    photoDialog.show();
	}
});

// Click sur Save (Insert ou Update)
saveButton.addEventListener('click', function() {
	Ti.API.info("saveButton");
	if (locationTextField.value == '') {
		alert(LOCATION_MANDATORY);
	}
	else {
		if (locationTextField.id) {
			if (imageHasBeenRemoved) {
				if (imageFilename) {
					imageFile = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory + '/' + directory, imageFilename);
					imageFile.deleteFile();
					imageFilename = '';
				}
			}
			
			if (imageHasChanged) {
				if (imageFilename) {
					imageFile = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory + '/' + directory, imageFilename);
					imageFile.deleteFile();
					imageFilename = '';
				}
				
				newImageFilename = nowYYYYMMMDD() + '.jpg';
				newImageFile = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory + '/' + directory, newImageFilename);
				newImageFile.write(photo.toImage());
				imageFilename = newImageFilename;
			}
			updateLocation(locationTextField.value, imageFilename, locationTextField.id);
		}
		else {
			if (imageHasChanged) {
				newImageFilename = nowYYYYMMMDD() + '.jpg';
				newImageFile = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory + '/' + directory, newImageFilename);
				newImageFile.write(photo.toImage());
			}
			else {
				newImageFilename = '';
			}
			insertLocation(locationTextField.value, newImageFilename);
		}
		currentWindow.close();
	}				
});

///////////////////////////////////////////////////////////////////


