currentWindow = Titanium.UI.currentWindow;
currentWindow.backgroundColor = 'white';

/*
currentWindow.orientationModes = [
	Titanium.UI.PORTRAIT,
	Titanium.UI.UPSIDE_PORTRAIT
]; 

*/
Ti.include('./language.js');

Ti.include("./dbQueries.js");

Ti.include("./utils.js");

////////////////////////////////////////////////////////////
// Navigation Bar with a cancel and save button

if (currentWindow.title == SELECT_LOCATION) {

	Ti.include('./cancelButton.js');

	cancelButton.addEventListener('click', function() {
/*
		if (currentWindow.navGroup) {
			currentWindow.navGroup.close(currentWindow);
		}
		else {

*/
			currentWindow.close();		
//		}
	});
	
}

Ti.include('./addButton.js');


////////////////////////////////////////////////////////////
//function displayTableView()
function reloadDataLocation()
{
    var db = Titanium.Database.open('wiwdb');
    var locations = db.execute('SELECT ROWID, LOCATION_NAME, LOCATION_IMAGE_FILENAME FROM LOCATIONS');

    var data = [];

    // create table view data object
    while (locations.isValidRow()) {

        var row = Ti.UI.createTableViewRow({
			height: rowHeightLocations
		});
		
		row.rowid = locations.fieldByName('ROWID');
		
		var location = Ti.UI.createLabel({
            text:locations.fieldByName('LOCATION_NAME'),
			font: fontStyle12,
			color:'#576996',
			top : topLocContact,
            left:leftAfterImage,
			width: widthMax,
			height:'auto'
		});

		location.text = locations.fieldByName('LOCATION_NAME');
		row.add(location);
		
        var photo = Ti.UI.createImageView({
			borderRadius:10,
            top:topPhoto,
            left:leftPhoto,
            width:imageSmallWidth,
            height:imageSmallHeight
       });

		var imageFile;
		if (locations.fieldByName('LOCATION_IMAGE_FILENAME')) {
			imageFile = Ti.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory + '/images_locations',locations.fieldByName('LOCATION_IMAGE_FILENAME'));
		}
		else {
			imageFile = Ti.Filesystem.getFile('./images/photo.png');			
		}
		photo.image = imageFile;

        row.add(photo);
		
		var nb = getNumberItemsByLocation(row.rowid);
		if (nb > 0) {
			var textItem;
			if (nb == 1) {
				textItem = ' ' + ITEM;
			}
			else {
				textItem = ' ' + ITEMS;
			}
			var nbItems = Ti.UI.createLabel({
				font: fontStyle12,
				color:'#576996',
				text: nb + textItem,
				left: widthMax-widthNb-rightNbItems,
				top: topLocContact,
				width: widthNb,
				height: 'auto',
				textAlign: 'right',
				clickName: 'nb'
			});
			row.add(nbItems);
		}

        data.push(row); 

        locations.next();
		}

    locations.close();
    db.close();

	if (currentWindow.title == LOCATIONS) {
		var rowNewEntry = Ti.UI.createTableViewRow();
		var newEntry = Ti.UI.createLabel({
			text: NEW_LOCATION + ' ...',
			font: fontStyle14,
			color:'#576996',
			clickName: 'new',
			editable : false,
			left: leftTypes,
			top: topTypes,
			width: widthMax,
			height:'auto',
			textAlign: 'left'
		});
		rowNewEntry.add(newEntry);
		data.push(rowNewEntry);
	}

	return data;
}


var dataLocations = reloadDataLocation();

// create table view
var tableview = Titanium.UI.createTableView({
	deleteButtonTitle:DELETE,
    data:dataLocations
    });

// add table view to the window
currentWindow.add(tableview);

if (currentWindow.title == SELECT_LOCATION) {
    tableview.addEventListener('click', function(e) {
        Ti.App.fireEvent('sendSelectedLocationEvent', {
        	selectedLocationId: e.row.rowid});	        
        currentWindow.close();
//		currentWindow.navGroup.close(currentWindow);
    });		
}

if (currentWindow.title == LOCATIONS) {
	tableview.editable = true;
		
	tableview.addEventListener('delete', function(e) {
		if (e.clickName != 'new') {
			deleteLocationByRowid(e.row.rowid);
		}
	});				
			
	tableview.addEventListener('click', function(e) {
		if (e.row) {
			
			if (e.source.clickName != 'nb') {
				var locationDetailWindow = Titanium.UI.createWindow({
					title: e.row.rowid ? LOCATION_DETAIL : NEW_LOCATION,
					url: 'locationDetail.js',
					locationId: e.row.rowid ? e.row.rowid : ''
				});
				Ti.UI.currentTab.open(locationDetailWindow, {modal:true});
				/*
				locationDetailWindow.open({
					modal: true
//					,
//					modalTransitionStyle: Ti.UI.iPhone.MODAL_TRANSITION_STYLE_FLIP_HORIZONTAL
				});
				*/
				locationDetailWindow.addEventListener('close', function(){
					var dataLocations = reloadDataLocation();
					tableview.setData(dataLocations);
				});
			}
		else if (e.source.clickName == 'nb') {
			Titanium.API.info('else if');
		    var win = Titanium.UI.createWindow({
//	        	backgroundColor:'#fff',
	        	url:'./items.js',
				title: ITEMS + ' ' + getLocationNameById(e.row.rowid),
				searchValueType: 'location',
				searchValue: e.row.rowid
		    });
	    	Ti.UI.currentTab.open(win, {modal:true});							
//	    	win.open({modal:true});							
			}

		}
	});								
}


currentWindow.addEventListener('focus', function() {
    var dataLocations = reloadDataLocation();
	tableview.setData(dataLocations);
});

add.addEventListener('click', function() {
	Ti.API.info('add click');
	if (currentWindow.title == SELECT_LOCATION) {
		Ti.API.info('add click SELECT_LOCATION');
	    var containerView = Titanium.UI.createView ({});
	
	    var addView = Titanium.UI.createView ({
	            width:280,
	            height:120,
	            top:5,
	            backgroundColor:'#708090',
	            borderColor:'white',
	            borderSize:3,
	            borderRadius:10
	    });
	    var label = Titanium.UI.createLabel({
	        text: NEW_LOCATION,
	        height:30,
	        top:3,
	        textAlign: "center",
	        font:{fontSize:16, fontStyle:'bold'},
	        color:'#fff'
	    });
	
	    var textField = Titanium.UI.createTextField({
	        height:30,
	        top:40,
	        width:260,
	        hintText:'Name',
	        borderStyle:Titanium.UI.INPUT_BORDERSTYLE_ROUNDED
	    });
	
	    setTimeout(function() {textField.focus();},100);	
	    
	    var cancelButton = Titanium.UI.createButton({
	        title:CANCEL,
	        height:30,
	        width:120,
	        top:80,
	        left:10
	    });
	    var OKButton = Titanium.UI.createButton({
	        title:'OK',
	        height:30,
	        width:120,
	        top:80,
	        right:10
	    });
		
	    addView.add(label);
	    addView.add(textField);
	    addView.add(cancelButton);
	    addView.add(OKButton);
	    
	    containerView.add(addView);    
	    currentWindow.add(containerView);
	    	
	    cancelButton.addEventListener('click', function() {		
	        currentWindow.remove(containerView);
	    });
	
	    OKButton.addEventListener('click', function() {		
	        var db = Titanium.Database.open('wiwdb');
	        db.execute('INSERT INTO LOCATIONS (LOCATION_NAME) VALUES(?)',textField.value);
	
			if (currentWindow.title == SELECT_LOCATION) {
				Ti.App.fireEvent('sendSelectedLocationEvent', {
					selectedLocationId: db.lastInsertRowId});
				currentWindow.close();
			}
			else 
				if (currentWindow.title == LOCATIONS) {
					currentWindow.remove(containerView);
				    var dataLocations = reloadDataLocation();
					tableview.setData(dataLocations);
				}
	        db.close();
	    });
	}

	if (currentWindow.title == LOCATIONS) {
		Ti.API.info('add button LOCATIONS');
	    var locationDetailWindow = Titanium.UI.createWindow({
	        title: NEW_LOCATION,
	        url: 'locationDetail.js'
	        });
	    Ti.UI.currentTab.open(locationDetailWindow, {modal:true});
/*
	    locationDetailWindow.open({
			modal:true,
			modalTransitionStyle:Ti.UI.iPhone.MODAL_TRANSITION_STYLE_COVER_VERTICAL
//			modalStyle:presentation,
//			navBarHidden:true
			});
	*/	
		locationDetailWindow.addEventListener('close', function() {
		    var dataLocations = reloadDataLocation();
			tableview.setData(dataLocations);
			});
	}
});

