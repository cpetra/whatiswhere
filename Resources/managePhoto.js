Ti.include('./language.js');

var imageHasChanged = 0;
var imageHasBeenRemoved = 0;

var photo = Titanium.UI.createImageView({
	left: 5,
	top : 5,
	height:imageMediumHeight,
	width:imageMediumWidth,
	borderRadius:10,
	clickName: 'addPhoto'
});
//photo.top = photoTop;

var photoL = Titanium.UI.createImageView({
	height:imageLargeHeight,
	width:imageLargeWidth
});

if (imageFilename) {
	var imageFile = Ti.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory + '/'+ directory,imageFilename);
	photo.image = imageFile;	
}

var modifyImageLabel = Titanium.UI.createLabel({
	text: MODIFY,
	textAlign:'center',
	font:{fontSize: 12, fontWeight: 'bold'},
	left: 5,
	top : 5 + imageMediumHeight - 14,
//	bottom: 5,
	height:'auto',
	width:imageMediumWidth,
	backgroundColor:'black',
	color:'white',
	borderRadius:10,
	opacity:0.7
});

var addImageLabel = Titanium.UI.createLabel({
//	backgroundImage:'./images/addPhotofr.png',
	text:ADD_PHOTO,
	textAlign:'center',
	font:{fontSize: 10, fontWeight: 'bold'},
	left: 5,
	height:imageMediumHeight,
	width:imageMediumWidth,
	backgroundColor:'#849CC4',		
	color:'white',
	borderRadius:10,
	clickName: 'addPhoto'
});

var photoDialog = Titanium.UI.createOptionDialog();

if (photo.image) {
	photoDialog.options = [SELECT_PHOTO, TAKE_PHOTO, DELETE_PHOTO, CANCEL];
	photoDialog.cancel = 3;
}
else {
	photoDialog.options = [SELECT_PHOTO, TAKE_PHOTO, CANCEL];
	photoDialog.cancel = 2;
}

/*
photo.addEventListener('click',function(e) {
    photoDialog.show();
});
addImageLabel.addEventListener('click',function(e) {
    photoDialog.show();
});
*/

photoDialog.addEventListener('click', function(e) {

	if (e.index == 0) {
//		var popoverView;
//		var arrowDirection;

		Titanium.Media.openPhotoGallery({
		
			success:function(event) {
				var image = event.media;
				
				if(event.mediaType == Ti.Media.MEDIA_TYPE_PHOTO) {
					photo.image = image;
					photoL.image = image;
					imageHasChanged = 1;
					rowName.remove(addImageLabel);
					rowName.add(modifyImageLabel);						
				}
				else {					
				}
								
			},
			cancel:function() {
			},
			error:function(error) {
			},
			allowEditing:false,
//			popoverView:popoverView,
//			arrowDirection:arrowDirection,
			showControls: true,
			mediaTypes:[Ti.Media.MEDIA_TYPE_VIDEO,Ti.Media.MEDIA_TYPE_PHOTO]
		});
	}
	else if (e.index == 1) {
		Titanium.Media.showCamera({

			success:function(event)
			{
				var image = event.media;
				
				// set image view
				if(event.mediaType == Ti.Media.MEDIA_TYPE_PHOTO) {
					photo.image = image;
					imageHasChanged = 1;
					rowName.remove(addImageLabel);
					rowName.add(modifyImageLabel);	
				}
				else {					
				}				
			},
			cancel:function()
			{
		
			},
			error:function(error)
			{
				// create alert
				var a = Titanium.UI.createAlertDialog({title:'Camera'});
		
				// set message
				if (error.code == Titanium.Media.NO_CAMERA)
				{
					a.setMessage('Device does not have video recording capabilities');
				}
				else
				{
					a.setMessage('Unexpected error: ' + error.code);
				}
		
				// show alert
				a.show();
			},
			saveToPhotoGallery:true,
			allowEditing:false
		});
	}
	else if (photo.image) {
		if (e.index == 2) {
				photo.image = null;
				imageHasBeenRemoved = 1;
				rowName.remove(modifyImageLabel);	
				rowName.add(addImageLabel);					
		}
		else if (e.index == 3) {
			
		}
	}
	else {
		if (e.index == 2 ) {
		}
	}
	
});

