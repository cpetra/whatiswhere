/**
 * this is a background service and this code will run *every* time the 
 * application goes into the foreground
 */

Ti.include("./utils.js");
Ti.include('./language.js');
Ti.include('./dbQueries.js');

		var now_s = Math.floor(new Date().getTime()/1000);

		var sortQuery = " ORDER BY I.REMINDER_DATE";
		var whereQuery = " WHERE REMINDER_DATE != '' AND REMINDER_DATE >= ?";
		var query = 'SELECT I.ROWID, I.NAME, T.TYPE_NAME, I.LOC_OR_CONTACT, L.LOCATION_NAME, L.LOCATION_IMAGE_FILENAME, I.CONTACT_ID, I.IMAGE_FILENAME, I.SINCE_DATE, I.REMINDER_DATE FROM ITEMS I LEFT OUTER JOIN TYPES T ON I.TYPE_ID = T.ROWID LEFT OUTER JOIN LOCATIONS L ON I.LOCATION_ID = L.ROWID ';
		query = query + whereQuery + sortQuery;
		var db = Titanium.Database.open('wiwdb');
		var items = db.execute(query, now_s);		
		
//Ti.API.info("nb items " + items.rowCount);
		var nbBadges = Titanium.UI.iPhone.appBadge;

		while (items.isValidRow()) {
		
			var itemName = items.fieldByName('NAME') != "" ? items.fieldByName('NAME') : NO_NAME;

			var whereName = items.fieldByName('LOC_OR_CONTACT') == "L" ? (items.fieldByName('LOCATION_NAME') ? items.fieldByName('LOCATION_NAME') : "") : ( items.fieldByName('CONTACT_ID') ? AT + items.fieldByName('CONTACT_ID') : "");

			var typeName = items.fieldByName('TYPE_NAME') ? items.fieldByName('TYPE_NAME') + ": " : "";

			var nbDays = items.fieldByName('SINCE_DATE') ? nbDaysBetween(new Date(), secondsToDate(items.fieldByName('SINCE_DATE'))) : '';			
			var textNbDays = SINCE + ' ' + nbDays + ' ' + (nbDays > 1 ? DAYS : DAY);

			var alertText = NOTIFICATION_REMIND + "\n"  + typeName  + itemName + "\n" + whereName + "\n" + textNbDays;
			
			var reminderDate = secondsToDate(items.fieldByName('REMINDER_DATE'));

//Ti.API.info(itemName + " " + reminderDate);

			if ( !Titanium.App.Properties.getString('alertsNotification') ) {
				Titanium.App.Properties.setString('alertsNotification', 1);
			}
			if ( !Titanium.App.Properties.getString('badgesNotification') ) {
				Titanium.App.Properties.setString('badgesNotification', 1);
			}
/*
			if ( !Titanium.App.Properties.getString("reminderHourNotification")) {
				Titanium.App.Properties.setString("reminderHourNotification",8);										
			}
			if ( !Titanium.App.Properties.getString("reminderMinuteNotification")) {
				Titanium.App.Properties.setString("reminderMinuteNotification",0);										
			}

*/
			if (Titanium.App.Properties.getString('alertsNotification') == 1) {
//			if (Titanium.App.Properties.getString('alerts_preference') == 1) {
// Ti.API.info("Alerts");
				var alertNotification = Ti.App.iOS.scheduleLocalNotification({
					alertBody: alertText,
					userInfo: {
						"itemId": items.fieldByName('ROWID'),
						"itemName": items.fieldByName('NAME')
					},

					alertAction:LAUNCH,
					date: reminderDate
//					date: reminderDate.setHours(Titanium.App.Properties.getString("reminderHourNotification"),Titanium.App.Properties.getString("reminderMinuteNotification"),0)
				});
			}			

			if (Titanium.App.Properties.getString('badgesNotification') == 1) {
// Ti.API.info("Badges");
//			if (Titanium.App.Properties.getString('badges_preference') == 1) {
				nbBadges = nbBadges + 1;
				
				Ti.App.iOS.scheduleLocalNotification({
//					alertBody : '',
					userInfo: {
						"itemId": items.fieldByName('ROWID')
					},

					date: reminderDate,
					badge : nbBadges
				});	

			}			
			items.next();
		}

		items.close();
		db.close();

/*
Ti.App.iOS.addEventListener('notification',function(e){
	Ti.API.info('background event received = '+alertNotification);
	Ti.API.info("local notification received: "+JSON.stringify(e));
//	Ti.App.currentService.unregister();
});

*/

/*
Ti.App.currentService.addEventListener('stop',function()
{
	Ti.API.info("background service is stopped");
});

*/
// we need to explicitly stop the service or it will continue to run
// you should only stop it if you aren't listening for notifications in the background
// to conserve system resources. you can stop like this:
//Ti.App.currentService.stop();


// you can unregister the service by calling 
// Ti.App.currentService.unregister() 
// and this service will be unregistered and never invoked again
