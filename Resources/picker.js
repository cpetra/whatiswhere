Ti.include('./language.js');

function displayPickerView(caller, date_s) {

	var containerPickerView = Ti.UI.createView({
//        width:300,
		height: 380,
        bottom:10,
        backgroundColor:'#708090',
        borderColor:'white',
        borderSize:3,
        borderRadius:10
	});

	
	var pickerTitleLabel = Ti.UI.createLabel({
        top:3,
        height:30,
        textAlign:"center",
        font:fontStyle16,
        color:'#fff'
 	});

    var OKButton = Titanium.UI.createButton({
        title:'OK',
        height:30,
        width:120,
		bottom: 60
//		right: 10
    });

    var noDateButton = Titanium.UI.createButton({
        title:'OK',
        height:30,
        width:120,
		bottom: 20
//		right: 10
    });

	var today = new Date();
	today.setHours(Titanium.App.Properties.getString("reminderHourNotification"),Titanium.App.Properties.getString("reminderMinuteNotification"),0);

	var picker = Ti.UI.createPicker({
		type:Ti.UI.PICKER_TYPE_DATE,
//		locale: Titanium.Platform.locale,
		top: 40,
		height: 250,
//		width: 200,
		value : today,
		selectionIndicator: true
	});


	if (caller == 'Since') {
		pickerTitleLabel.text = SINCE;

		noDateButton.title = NO_DATE;

		picker.maxDate = today;
/*
		if ( date_s ) {
			picker.value = secondsToDate(date_s);					
		}
		else {
			picker.value = today;			
			sinceTextField.value = TODAY;
			sinceTextField.date_s = Math.floor(today.getTime()/1000);			
		}

*/	}
	else if (caller == 'Reminder') {
		pickerTitleLabel.text = NB_DAYS_REMINDER;		

		noDateButton.title = NEVER;		
/*
		var tomorrow = new Date();
		tomorrow.setDate(today.getDate()+1);
		tomorrow.setHours(Titanium.App.Properties.getString("reminderHourNotification"),Titanium.App.Properties.getString("reminderMinuteNotification"),0);

*/		picker.minDate = today;		
	}

	if ( date_s ) {
		picker.value = secondsToDate(date_s);							
	}
	else {
		picker.value = today;
		reminderTextField.value = TODAY;
		reminderTextField.date_s = Math.floor(today.getTime()/1000);
	}

	containerPickerView.add(pickerTitleLabel);
	containerPickerView.add(picker);
	containerPickerView.add(OKButton);
	containerPickerView.add(noDateButton);

	currentWindow.hideNavBar();		

	currentWindow.add(containerPickerView);
	
	picker.addEventListener('change',function(e) {
			e.value.setHours(Titanium.App.Properties.getString("reminderHourNotification"),Titanium.App.Properties.getString("reminderMinuteNotification"),0);
//			Ti.API.info('selected ' + e.value);			
		if (caller == 'Since') {
			sinceTextField.date_s = e.value.getTime()/1000;						
			sinceTextField.value = e.value.toLocaleDateString();					
		}
		else if (caller == 'Reminder') {
			reminderTextField.date_s = e.value.getTime()/1000;						
			reminderTextField.value = e.value.toLocaleDateString();	
		}
	});

	OKButton.addEventListener('click',function(e) {
		currentWindow.remove(containerPickerView);
		currentWindow.showNavBar();
	});

	noDateButton.addEventListener('click',function(e) {		
		if (caller == 'Since') {
			sinceTextField.date_s = '';						
			sinceTextField.value = NO_DATE;					
		}
		else if (caller == 'Reminder') {
			reminderTextField.date_s = '';						
			reminderTextField.value = NEVER;	
		}
		currentWindow.remove(containerPickerView);
		currentWindow.showNavBar();
	});
}

function displayTimePickerView(hour, minute){
	var containerPickerView = Ti.UI.createView({
//        width:300,
		height: 380,
        bottom:10,
        backgroundColor:'#708090',
        borderColor:'white',
        borderSize:3,
        borderRadius:10
	});

	
	var pickerTitleLabel = Ti.UI.createLabel({
        top:3,
        height:30,
        textAlign:"center",
        font:fontStyle16,
        color:'#fff',
		text: REMINDER_TIME
 	});

    var OKButton = Titanium.UI.createButton({
        title:'OK',
        height:30,
        width:120,
		bottom: 60
//		right: 10
    });

    var cancelButton = Titanium.UI.createButton({
        title:CANCEL,
        height:30,
        width:120,
		bottom: 20
//		right: 10
    });

	var picker = Ti.UI.createPicker({
		type:Ti.UI.PICKER_TYPE_TIME,
//		locale: Titanium.Platform.locale,
		top: 40,
		height: 250,
//		width: 200,
		selectionIndicator: true
	});

	var value = new Date();
	value.setHours(hour, minute, 0);

	picker.value = value;
	
	containerPickerView.add(pickerTitleLabel);
	containerPickerView.add(picker);
	containerPickerView.add(OKButton);
	containerPickerView.add(cancelButton);

	currentWindow.hideNavBar();		

	currentWindow.add(containerPickerView);

	picker.addEventListener('change',function(e) {
		timeTextField.value = (e.value.getHours() < 10 ? "0" + e.value.getHours() : e.value.getHours() ) + ":" + (e.value.getMinutes() < 10 ? "0" + e.value.getMinutes() : e.value.getMinutes());
		timeTextField.alertLaunchHour = e.value.getHours();
		timeTextField.alertLaunchMinute = e.value.getMinutes();
		Titanium.App.Properties.setString("reminderHourNotification",e.value.getHours());			
		Titanium.App.Properties.setString("reminderMinuteNotification",e.value.getMinutes());			
//		e.value.setHours(alertLaunchHour,alertLaunchMinute,0);
				
//			Ti.API.info('selected ' + e.value);	
	});		

	OKButton.addEventListener('click',function(e) {		
		updateItemsReminderTime();		
		currentWindow.remove(containerPickerView);
		currentWindow.showNavBar();
	});

	cancelButton.addEventListener('click',function(e) {	
		timeTextField.value = ( hour < 10 ? "0" + hour : hour) + ":" + ( minute < 10 ? "0" + minute : minute) ;			
		timeTextField.alertLaunchHour = hour;
		timeTextField.alertLaunchMinute = minute;
		Titanium.App.Properties.setString("reminderHourNotification",hour);			
		Titanium.App.Properties.setString("reminderMinuteNotification",minute);			
		currentWindow.remove(containerPickerView);
		currentWindow.showNavBar();
	});

}