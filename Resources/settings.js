currentWindow = Titanium.UI.currentWindow;
currentWindow.backgroundColor = 'white';

Ti.include('./language.js');

Ti.include("./dbQueries.js");

Ti.include("./utils.js");

Ti.include("./picker.js");

var data = [];
var tableView = Ti.UI.createTableView({
	style: Titanium.UI.iPhone.TableViewStyle.GROUPED
});

var rowBadges = Ti.UI.createTableViewRow({
	header : NOTIFICATIONS
	});
var badgesLabel = Titanium.UI.createLabel({
	font: fontStyle14,
    left: 10,
	text:BADGES,
	borderStyle:Titanium.UI.INPUT_BORDERSTYLE_NONE
});
rowBadges.add(badgesLabel);

var badgeValue = Titanium.App.Properties.getString("badgesNotification") ? Titanium.App.Properties.getString("badgesNotification") : true;

var badgesSwitch = Ti.UI.createSwitch({
	value : badgeValue,
	right : 10
});
rowBadges.add(badgesSwitch);
data.push(rowBadges);


var rowAlerts = Ti.UI.createTableViewRow({
});
var alertsLabel = Titanium.UI.createLabel({
	font: fontStyle14,
    left: 10,
	text:ALERTS,
	borderStyle:Titanium.UI.INPUT_BORDERSTYLE_NONE
});
rowAlerts.add(alertsLabel);

var alertValue = Titanium.App.Properties.getString("alertsNotification") ? Titanium.App.Properties.getString("alertsNotification") : true;

var alertsSwitch = Ti.UI.createSwitch({
	value : alertValue,
	right : 10
});
rowAlerts.add(alertsSwitch);
data.push(rowAlerts);


var rowReminderTime = Ti.UI.createTableViewRow({
	hasChild:true
});
var reminderTimeLabel = Titanium.UI.createLabel({
	font: fontStyle14,
    left: 10,
	text:REMINDER_TIME,
	borderStyle:Titanium.UI.INPUT_BORDERSTYLE_NONE
});

var alertLaunchHour = Titanium.App.Properties.getString("reminderHourNotification") ? Titanium.App.Properties.getString("reminderHourNotification") : 8;
var alertLaunchMinute = Titanium.App.Properties.getString("reminderMinuteNotification") ? Titanium.App.Properties.getString("reminderMinuteNotification") : 0;

var timeTextField = Titanium.UI.createTextField({
	font: fontStyle14,
	color:'#576996',
	textAlign: 'right',
	paddingRight:10,
    enabled : false,
	borderStyle:Titanium.UI.INPUT_BORDERSTYLE_NONE,
	alertLaunchHour : alertLaunchHour,
	alertLaunchMinute: alertLaunchMinute,
	value : alertLaunchHour + ":" + alertLaunchMinute
});

timeTextField.value = (alertLaunchHour < 10 ? "0" + alertLaunchHour : alertLaunchHour ) + ":" + (alertLaunchMinute < 10 ? "0" + alertLaunchMinute : alertLaunchMinute);

rowReminderTime.add(reminderTimeLabel);
rowReminderTime.add(timeTextField);
data.push(rowReminderTime);


tableView.data = data;
currentWindow.add(tableView);


rowReminderTime.addEventListener('click', function(e) {
	displayTimePickerView(timeTextField.alertLaunchHour, timeTextField.alertLaunchMinute);
});


badgesSwitch.addEventListener('change', function(e)
{
	Titanium.App.Properties.setString("badgesNotification",e.value);
//	Ti.API.info("Badges" + e.value);
//	Ti.API.info("Badges" + Titanium.App.Properties.getString("badgesNotification"));
});

alertsSwitch.addEventListener('change', function(e)
{
	Titanium.App.Properties.setString("alertsNotification",e.value);
//	Ti.API.info("Alerts" + e.value);
//	Ti.API.info("Badges" + Titanium.App.Properties.getString("alertsNotification"));
});

var logo = Titanium.UI.createImageView({
//	left: 5,
	bottom : 44,
	height:60,
	width:60,
	borderRadius:10,
	image: Ti.Filesystem.getFile('./default_app_logo.png')
});
currentWindow.add(logo);

var label = Titanium.UI.createLabel ({
	text : "Version " + Ti.App.getVersion() + "\nGuillaume Lévy",
	font : fontStyle12,
	color : 'gray',
	bottom : 10,
	height:'auto',
	textAlign : 'center'
});
currentWindow.add(label);

 