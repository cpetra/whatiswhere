
var sortButton = Titanium.UI.createButton({
    title:SORTBY,
    style:Titanium.UI.iPhone.SystemButtonStyle.PLAIN
});

currentWindow.setLeftNavButton(sortButton);

var sortDialog = Titanium.UI.createOptionDialog({
	title: SORTBY,
	options: [NAME, TYPE, CONTACT + ', ' + LOCATION, CREATION_DATE, SINCE_DATE, CANCEL],
	cancel: 5
});


sortButton.addEventListener('click', function() {
	sortDialog.show();
});

sortDialog.addEventListener('click', function(e) {
	
	switch (e.index) {
		case 0:
		sortType = 'item';
		break;
		case 1:
		sortType = 'type';
		break;
		case 2:
		sortType = 'location';
		break;
		case 3:
		sortType = 'creationDate';
		break;
		case 4:
		sortType = 'sinceDate';
		break;
		case 'default':
		sortType = '';
		break;				
	}
	var updatedData = reloadData();
	tableview.setData(updatedData);
});
