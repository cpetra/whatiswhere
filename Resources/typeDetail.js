currentWindow = Titanium.UI.currentWindow;

Ti.include("./dbQueries.js");

Ti.include('./language.js');

Ti.include("./utils.js");

////////////////////////////////////////////////////////////
// Navigation Bar with a cancel and save button

Ti.include('./cancelButton.js');

cancelButton.addEventListener('click', function() {
	currentWindow.close();
});


Ti.include('./saveButton.js');
////////////////////////////////////////////////////////////

var data = [];
var tableView = Ti.UI.createTableView({
	moving:false, 	
	scrollable: false,
	top: 0,
	style: Titanium.UI.iPhone.TableViewStyle.GROUPED
});


////////////////////////////////////////////////////////////

data[0] = Ti.UI.createTableViewSection();

var rowType = Ti.UI.createTableViewRow({
	//height:'auto',
	height: '30dp',
	backgroundColor: 'blue'
	});
var typeTextField = Titanium.UI.createTextField({
	font: fontStyle14,
	color:'#576996',
    left: 10,
	hintText:ITS_NAME,
	borderStyle:Titanium.UI.INPUT_BORDERSTYLE_NONE
});

setTimeout(function() {typeTextField.focus();},100);

// Add Type Text Field
rowType.add(typeTextField);
data[0].add(rowType);

typeTextField.id = currentWindow.typeId;
typeTextField.value = getTypeNameById(currentWindow.typeId);


////////////////////////////////////////////////////////////
tableView.data = data;
currentWindow.add(tableView);
////////////////////////////////////////////////////////////


// Click sur Save (Insert ou Update)
saveButton.addEventListener('click', function() {
	if (typeTextField.value == '') {
		alert(TYPE_MANDATORY);
	}
	else {
		if (typeTextField.id) {
			updateType(typeTextField.value, currentWindow.typeId);
		}
		else {
			insertType(typeTextField.value);
		}
		currentWindow.close();
	}
});

///////////////////////////////////////////////////////////////////


