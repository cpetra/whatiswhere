currentWindow = Titanium.UI.currentWindow;
currentWindow.backgroundColor = 'white';

/*
currentWindow.orientationModes = [
	Titanium.UI.PORTRAIT,
	Titanium.UI.UPSIDE_PORTRAIT
]; 

*/
Ti.include("./dbQueries.js");

Ti.include('./language.js');

Ti.include("./utils.js");

////////////////////////////////////////////////////////////
// Navigation Bar with a cancel and save button

if (currentWindow.title == SELECT_TYPE) {

 	Ti.include('./cancelButton.js');

	cancelButton.addEventListener('click', function() {
/*
		if (currentWindow.navGroup) {
//			currentWindow.navGroup.close(currentWindow.parentWindow);
			currentWindow.navGroup.close(currentWindow);
		}
		else {

*/
			currentWindow.close();		
//		}
	});	

}

Ti.include('./addButton.js');

////////////////////////////////////////////////////////////

function reloadDataType()
{
    var db = Titanium.Database.open('wiwdb');
    var types = db.execute('SELECT ROWID, TYPE_NAME FROM TYPES');
    var data = [];

    // create table view data object
    while (types.isValidRow()) {

        var row = Ti.UI.createTableViewRow({
			height: rowHeightTypes
		});
		
		row.rowid = types.fieldByName('ROWID');
		
		var containerView = Ti.UI.createView({
//			top: 20,
//			left: 6			
		});
		
		var type = Ti.UI.createLabel({
            text:types.fieldByName('TYPE_NAME'),
			font: fontStyle12,
			color:'#576996',
			left: leftTypes,
			top: topTypes,
			width: widthMax,
			height:'auto',
			textAlign: 'left'
			});
		containerView.add(type);		
//		row.add(type);
	
		var nb = getNumberItemsByType(row.rowid);
		if (nb > 0) {
			var textItem;
			if (nb == 1) {
				textItem = ' ' + ITEM;
			}
			else {
				textItem = ' '+ ITEMS;
			}
			var nbItems = Ti.UI.createLabel({
				text: nb + textItem,
				font: fontStyle12,
				color:'#576996',
				left: widthMax-widthNb-rightNbItems,
				top: topTypes,
				width: widthNb,
				height: 'auto',
				textAlign: 'right',
				clickName: 'nb'
			});
			containerView.add(nbItems);
//			row.add(nbItems);
		}

		row.add(containerView);
		
        data.push(row); 

        types.next();
		}

    types.close();
    db.close();

	if (currentWindow.title == TYPES) {
		var rowNewEntry = Ti.UI.createTableViewRow();
		var newEntry = Ti.UI.createLabel({
			text: NEW_TYPE + ' ...',
			font: fontStyle14,
			color: '#576996',
			clickName: 'new',
			left: leftTypes,
			top: topTypes,
			width: widthMax,
			height:'auto',
			textAlign: 'left'
		});
		rowNewEntry.add(newEntry);
		data.push(rowNewEntry);
	}

	return data;
}


var dataTypes = reloadDataType();

// create table view
var tableview = Titanium.UI.createTableView({
	deleteButtonTitle:DELETE,
    data:dataTypes
});

// add table view to the window
currentWindow.add(tableview);

if (currentWindow.title == SELECT_TYPE) {
    tableview.addEventListener('click', function(e) {
        Ti.App.fireEvent('sendSelectedTypeEvent', {
        	selectedTypeId: e.row.rowid});	        
        currentWindow.close();
//		currentWindow.navGroup.close(currentWindow.parent);
//		currentWindow.navGroup.close(currentWindow);
    });		
}

if (currentWindow.title == TYPES) {
	tableview.editable = true;
	
	tableview.addEventListener('delete', function(e) {
		if (e.clickName != 'new') {
			deleteTypeByRowid(e.row.rowid);			
		}
	});				
	
	tableview.addEventListener('click', function(e) {
		if (e.row) {
			if (e.source.clickName != 'nb') {
			    var typeDetailWindow = Titanium.UI.createWindow({
			        title: e.row.rowid ? TYPE_DETAIL : NEW_TYPE,
			        url: 'typeDetail.js',
					typeId: e.row.rowid ? e.row.rowid : ''
			        });
				Ti.UI.currentTab.open(typeDetailWindow, {modal:true});
			    //typeDetailWindow.open({modal:true});
				
				typeDetailWindow.addEventListener('close', function() {
				    var dataTypes = reloadDataType();
					tableview.setData(dataTypes);
					});					
				}
		else if (e.source.clickName == 'nb') {
		    var win = Titanium.UI.createWindow({
	        	url:'./items.js',
				title: ITEMS + ' ' + getTypeNameById(e.row.rowid),
				searchValueType: 'type',
				searchValue: e.row.rowid
		    	});
	    	Ti.UI.currentTab.open(win, {modal:true});							
			}
		}
	});				
}


currentWindow.addEventListener('focus', function() {
    var dataTypes = reloadDataType();
	tableview.setData(dataTypes);
});



function addOrUpdateTypeDialog() {
    var containerView = Titanium.UI.createView ({    
    });

    var addView = Titanium.UI.createView ({
            width:280,
            height:120,
            top:5,
            backgroundColor:'#708090',
            borderColor:'white',
            borderSize:3,
            borderRadius:10
    });

    var label = Titanium.UI.createLabel({
        text: NEW_TYPE,
        height:30,
        top:3,
        textAlign: "center",
        font:fontStyle16,
        color:'#fff'
    });

    var textField = Titanium.UI.createTextField({
        height:30,
        top:40,
        width:260,
        hintText:NAME,
        borderStyle:Titanium.UI.INPUT_BORDERSTYLE_ROUNDED
    });

    setTimeout(function() {textField.focus();},100);

    
    var cancelButton = Titanium.UI.createButton({
        title:CANCEL,
        height:30,
        width:120,
        top:80,
        left:10
    });
    var OKButton = Titanium.UI.createButton({
        title:'OK',
        height:30,
        width:120,
        top:80,
        right:10
    });

    addView.add(label);
    addView.add(textField);
    addView.add(cancelButton);
    addView.add(OKButton);
    
    containerView.add(addView);    
    currentWindow.add(containerView);
    

    cancelButton.addEventListener('click', function()
    {
        currentWindow.remove(containerView);
    });

    OKButton.addEventListener('click', function()
    {
		var insertedRowId = insertType(textField.value);
 
 		if (currentWindow.title == SELECT_TYPE) {
	        Ti.App.fireEvent('sendSelectedTypeEvent', {
	        	selectedTypeId: insertedRowId
				});        
	        currentWindow.close();
		}
		else if (currentWindow.title == TYPES) {
	        currentWindow.remove(containerView);
		    var dataTypes = reloadDataType();
			tableview.setData(dataTypes);
		}
    });
	
}

add.addEventListener('click', function()
{
	if (currentWindow.title == SELECT_TYPE) {
		addOrUpdateTypeDialog();
	}

	if (currentWindow.title == TYPES) {
	    var typeDetailWindow = Titanium.UI.createWindow({
	        title: NEW_TYPE,
	        url: 'typeDetail.js'
	        });
		Ti.UI.currentTab.open(typeDetailWindow, {modal:true});
		/*
	    typeDetailWindow.open({
			modal:true,
			modalTransitionStyle:Ti.UI.iPhone.MODAL_TRANSITION_STYLE_COVER_VERTICAL
			});
		*/		
		typeDetailWindow.addEventListener('close', function() {
		    var dataTypes = reloadDataType();
			tableview.setData(dataTypes);
			});
	}

});

