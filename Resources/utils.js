function secondsToLocaleDateString(date_s) {
	var date = new Date(date_s*1000).toLocaleDateString();
	return date;
}

function secondsToDate(date_s) {
	var date = new Date(date_s*1000);
	return date;
}

function nowYYYYMMMDD() {
	var date = new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDay();
	var hour = date.getHours();
	var minute = date.getMinutes();
	var second = date.getSeconds();
	
	var nowFormatted = year.toString() + month + day + hour + minute + second;
	
	return nowFormatted;
}

function nowPlusDays(nbDays) {
	var nbDays_ms = nbDays * 1000 * 60 * 60 * 24;
	var today = new Date();
	var today_ms = today.getTime();
	
	var newDate_ms = today_ms + nbDays_ms;	

	var newDate = new Date(newDate_ms);

	return newDate;
}

function nbDaysBetween(date1, date2) {
 
    var oneDay = 1000 * 60 * 60 * 24;
	
    var date1_ms = date1.getTime();
    var date2_ms = date2.getTime();

    var difference_ms = Math.abs(date1_ms - date2_ms);

    return Math.floor(difference_ms/oneDay);
}

var fontSizeText = 12;
var rowHeightItems = 52;
var rowHeightTypes = 40;
var rowHeightLocations = 50;
var rowHeight = 35;
var widthMax = 320;
var topTypes = 12;
var topLocContact = 18;
var topPhoto = 6;
var topFirstLine = 6;
var topDisplay = 20;
var leftTypes = 10;
var leftPhoto = 6;
var rightPhoto = 6;
var topContactActions = 38;
var leftContactActions = 2;
var rightContactActions = 2;
var widthContactActions = 48;
var imageSmallWidth = 40;
var imageSmallHeight = 40;
var imageMediumHeight = 70;
var imageMediumWidth = 70;
var imageLargeHeight = 200;
var imageLargeWidth = 200;
var rightAfterImage = 54;
var leftAfterImage = 54;
var widthNb = 70;
var leftNbItems = 10;
var rightNbItems = 10;
var fontStyle16 = {fontSize:16,fontWeight:'bold'};
var fontStyle14 = {fontSize:14,fontWeight:'bold'};
var fontStyle14Color = {fontSize:14,fontWeight:'bold', color:'#576996'};
var fontStyle12 = {fontSize:12,fontWeight:'bold'};
var fontStyle12Color = {fontSize:12,fontWeight:'bold', color:'#576996'};
var fontStyle10 = {fontSize:10,fontWeight:'bold'};
var fontStyle10Color = {fontSize:10,fontWeight:'bold', color:'#576996'};
var fontStyle8 = {fontSize:8,fontWeight:'bold'};
//var alertLaunchHour = Titanium.App.Properties.getString("reminderHourNotification") ? Titanium.App.Properties.getString("reminderHourNotification") : 8;
//var alertLaunchMinute = Titanium.App.Properties.getString("reminderMinuteNotification") ? Titanium.App.Properties.getString("reminderMinuteNotification") : 0;